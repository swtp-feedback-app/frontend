FROM caddy:alpine
COPY build/web /usr/share/caddy/
CMD caddy file-server -root /usr/share/caddy/
