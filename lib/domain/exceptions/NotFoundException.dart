class NotFoundException implements Exception {
  String message;

  NotFoundException.msg(String msg)
      : this.message = msg + ' konnte nicht gefunden werden.';

  NotFoundException()
      : this.message =
            'Die angeforderte Ressource konnte nicht gefunden werden.';

  String toString() {
    return "NotFoundException: $message";
  }
}
