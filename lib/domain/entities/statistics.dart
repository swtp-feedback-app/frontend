class Statistics {
  int answered;
  int correct;
  int questionCount;

  Statistics({this.answered, this.correct});

  Statistics.fromJson(Map<String, dynamic> json) {
    questionCount = json['questionCount'];
    answered = json['answeredQuestions'];
    correct = json['correctQuestions'];
  }

  String getResult() {
    return ((correct / questionCount) * 100).round().toString();
  }
}

class UserStatistics {
  int courses;
  int sessions;
  int answQuestions;
  int corrQuestions;
  Map<String, double> chartData;

  get corrPercent => ((corrQuestions / answQuestions) * 100).round();

  UserStatistics.fromJson(Map<String, dynamic> json) {
    this.courses = json["courseCount"];
    this.sessions = json["sessionCount"];
    this.answQuestions = json["answeredQuestions"];
    this.corrQuestions = json["correctQuestions"];
    if (answQuestions == null) answQuestions = 0;
    if (corrQuestions == null) corrQuestions = 0;
    chartData = new Map();
    chartData.addAll({
      'Richtig': corrQuestions.toDouble(),
      'Falsch': (answQuestions - corrQuestions).toDouble(),
    });
  }
}

class SessionStatisticsOverview {
  int participants;
  int completed;
  Map<String, double> chartData;

  SessionStatisticsOverview.fromJson(Map<String, dynamic> json) {
    chartData = new Map();
    participants = json['participantCount'];
    completed = json['completeCount'];
    double percent100 = json['100pCorrect'].toDouble();
    double percent75 = json['75pCorrect'].toDouble() - percent100;
    double percent50 = json['50pCorrect'].toDouble() - percent100 - percent75;
    double under50 = completed - (percent50 + percent75 + percent100);
    chartData.addAll({
      '100%': percent100,
      '75 - 99,9%': percent75,
      '50 - 74,9%': percent50,
      '  0 - 49,9%': under50,
    });
  }
}
