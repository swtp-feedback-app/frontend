import 'question/question.dart';

class Quiz {
  int id;
  String name;
  String description;
  int questionCount;
  bool survey;
  bool instantFeedback;
  List<Question> questions = new List<Question>();

  Quiz({
    this.id,
    this.name,
    this.description,
    this.questionCount,
    this.survey,
    this.instantFeedback,
  });

  Quiz.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        description = json['description'],
        questionCount = json['questionCount'],
        survey = json['type'] == 'survey',
        instantFeedback = json['instantFeedback'];

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (id != null) data['id'] = this.id;
    data['name'] = this.name;
    data['description'] = this.description;
    data['questionCount'] = this.questionCount;
    data['type'] = this.survey ? "survey" : "quiz";
    data['instantFeedback'] = this.instantFeedback;
    return data;
  }

  @override
  String toString() {
    return 'Quiz id: $id, name: $name, description: $description, questionCount: $questionCount, survey? $survey, instantFeedback: $instantFeedback;';
  }
}
