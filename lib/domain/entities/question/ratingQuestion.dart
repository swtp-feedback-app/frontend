import 'answer.dart';
import 'question.dart';

class RatingQuestion implements Question {
  int number;
  String question;
  int min = 1; // inclusive minimum for scale
  int max = 5; // inclusive maximum for scale
  List<RatingAnswer> answers = new List(1);

  RatingQuestion(
      {this.number, this.question, this.min = 1, this.max = 5, this.answers});

  RatingQuestion.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    question = json['question']['question'];
    min = json['question']['answers']['min'];
    max = json['question']['answers']['max'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    final Map<String, dynamic> questionData = new Map<String, dynamic>();
    data['type'] = 'rating';
    if (number != null) {
      data['number'] = this.number;
    }
    questionData['question'] = this.question;
    questionData['answers'] = {'min': min, 'max': max};
    data['question'] = questionData;
    return data;
  }

  @override
  String toString() {
    return 'Rating Question number: $number, question: $question';
  }
}

class RatingAnswer implements Answer {
  String answer; // string representation of number e.g. 'trifft voll zu'
  int number; //value of 'min to max' e.g. 1 for 'trifft voll zu'

  RatingAnswer(this.number, this.answer);

  int toJson() {
    return number;
  }

  @override
  String toString() {
    return 'Answer: $answer';
  }
}
