import 'question.dart';
import 'answer.dart';

class SortQuestion implements Question {
  int number;
  String question;
  List<SortAnswer> answers;

  SortQuestion({this.number, this.question, this.answers});

  SortQuestion.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    question = json['question']['question'];
    if (json['question']['answers'] != null) {
      answers = answersFromJson(json['question']['answers']);
    }
  }

  List<SortAnswer> answersFromJson(List<dynamic> json) {
    return json.map((e) => SortAnswer.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    final Map<String, dynamic> questionData = new Map<String, dynamic>();
    if (number != null) {
      data['number'] = this.number;
    }
    data['type'] = 'sort';
    questionData['question'] = this.question;
    questionData['answers'] = answers.map((e) => e.toJson()).toList();
    data['question'] = questionData;
    return data;
  }

  @override
  String toString() {
    return 'Sort Question number: $number, question: $question, answers: $answers';
  }
}

class SortAnswer implements Answer {
  String answer;

  SortAnswer(String answer) : this.answer = answer;

  SortAnswer.fromJson(String json) {
    answer = json;
  }

  String toJson() {
    return answer;
  }

  @override
  String toString() {
    return 'Answers: $answer';
  }
}
