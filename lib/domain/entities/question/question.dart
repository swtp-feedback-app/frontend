import 'index.dart';

abstract class Question {
  int number;
  String question;
  covariant List<Answer> answers;

  Map<String, dynamic> toJson();

  static Question fromJson(Map<String, dynamic> json) {
    var type = json['type'];
    switch (type) {
      case 'multipleChoice':
        return MultipleChoiceQuestion.fromJson(json);
      case 'open':
        return OpenQuestion.fromJson(json);
      case 'sort':
        return SortQuestion.fromJson(json);
      case 'pairs':
        return PairsQuestion.fromJson(json);
      case 'wordCloud':
        return WordCloudQuestion.fromJson(json);
      case 'rating':
        return RatingQuestion.fromJson(json);
      default:
        throw UnimplementedError(
            'Questions with type $type are yet unimplemented.'); // <- Programmierfehler
    }
  }
}
