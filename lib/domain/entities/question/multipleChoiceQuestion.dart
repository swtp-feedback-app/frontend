import 'question.dart';
import 'answer.dart';

class MultipleChoiceQuestion implements Question {
  int number;
  String question;
  List<MultipleChoiceAnswer> answers;

  MultipleChoiceQuestion({this.number, this.question, this.answers});

  MultipleChoiceQuestion.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    question = json['question']['question'];
    if (json['question']['answers'] != null) {
      answers = answersFromJson(json['question']['answers']['options']);
    }
  }

  List<MultipleChoiceAnswer> answersFromJson(List<dynamic> json) {
    return json.map((e) => MultipleChoiceAnswer.fromJson(e)).toList();
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map();
    final Map<String, dynamic> questionData = new Map();
    final Map<String, dynamic> answerData = new Map();
    data['type'] = 'multipleChoice';
    if (number != null) {
      data['number'] = this.number;
    }
    questionData['question'] = this.question;
    answerData['options'] = this.answers.map((e) => e.toJson()).toList();
    questionData['answers'] = answerData;
    data['question'] = questionData;

    return data;
  }

  @override
  String toString() {
    return 'MC Question number: $number, question: $question, answers: $answers';
  }
}

class MultipleChoiceAnswer implements Answer {
  String answer;
  bool correct;
  bool chosen = false;

  MultipleChoiceAnswer(answer, [correct])
      : this.answer = answer,
        this.correct = correct;

  MultipleChoiceAnswer.fromJson(Map<String, dynamic> json)
      : answer = json['answer'],
        correct = json['correct'];

  dynamic toJson() {
    if (correct == null) {
      return answer;
    } else {
      final Map<String, dynamic> data = new Map();
      data['answer'] = answer;
      data['correct'] = correct;
      return data;
    }
  }

  @override
  String toString() {
    return 'Answer: $answer';
  }
}
