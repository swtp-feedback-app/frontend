import 'question.dart';
import 'answer.dart';

class WordCloudQuestion implements Question {
  int number;
  String question;
  List<WordCloudAnswer> answers = [new WordCloudAnswer("")];
  int max = 1; // maximum possible answers

  WordCloudQuestion({this.number, this.question, this.max = 1});

  WordCloudQuestion.fromJson(Map<String, dynamic> json) {
    number = json['number'];
    question = json['question']['question'];
    max = json['question']['answers']['max'];
  }

  @override
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    final Map<String, dynamic> questionData = new Map<String, dynamic>();
    data['type'] = 'wordCloud';
    if (number != null) {
      data['number'] = this.number;
    }
    questionData['question'] = this.question;
    questionData['answers'] = {'max': max};
    data['question'] = questionData;
    return data;
  }

  @override
  String toString() {
    return 'WordCloud Question number: $number, question: $question, max: $max';
  }
}

class WordCloudAnswer implements Answer {
  String answer;

  WordCloudAnswer(String answer) : this.answer = answer;

  String toJson() {
    return answer;
  }

  @override
  String toString() {
    return 'Answer: $answer';
  }
}
