import 'package:fbs_app/domain/entities/token.dart';

class User {
  int id;
  String username;
  String firstname;
  String lastname;
  bool docent;
  Token authToken;

  //Typically called from data_source layer after getting data from external source.
  User.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    username = json['username'];
    firstname = json['firstname'];
    lastname = json['lastname'];
    docent = json["docent"];
  }

  //Typically called from service or data_source layer just before persisting data.
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['username'] = this.username;
    data['firstname'] = this.firstname;
    data['lastname'] = this.lastname;
    data['docent'] = this.docent;
    return data;
  }

  @override
  String toString() {
    var first = firstname == null ? username : firstname;
    var last = lastname == null ? "" : lastname;
    return "$first $last";
  }
}
