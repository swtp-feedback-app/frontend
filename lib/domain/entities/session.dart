import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/domain/entities/token.dart';
import 'package:fbs_app/domain/entities/user.dart';
import 'package:intl/intl.dart';
import 'quiz.dart';

class Session {
  int id;
  Quiz quiz;
  Token sessToken;
  int start;
  int end;
  String type; // anonym or authenticated
  int beginAtQuestion = 0;
  User creator;
  bool complete;
  String description = "";
  Course course;

  //returns startDate as a formatted string
  get startDate => (DateFormat('dd.MM.yyyy HH:mm').format(
      DateTime.fromMillisecondsSinceEpoch((this.start * 1000).round(),
              isUtc: true)
          .toLocal()));

  //returns startDate as a formatted string
  get endDate => (DateFormat('dd.MM.yyyy HH:mm').format(
      DateTime.fromMillisecondsSinceEpoch((this.end * 1000).round(),
              isUtc: true)
          .toLocal()));

  //called from service-layer when either creating or editing a session
  Session(
      {int id,
      Quiz quiz,
      int start,
      int end,
      String description,
      String type}) {
    this.id = id;
    this.quiz = quiz;
    this.type = type;
    this.start = start;
    this.end = end;
    this.description = description;
  }

  //Typically called from data_source layer after getting data from external source.
  Session.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    if (json['quiz'] != null) {
      quiz = Quiz.fromJson(json['quiz']);
    }
    start = json['start'];
    end = json['end'];
    type = json['type'];
    if (json['creator'] != null) {
      creator = User.fromJson(json['creator']);
    }
    complete = json['complete'];
    description = json['description'];
    // added for SessionDetailScreen
    if (json['course'] != null) {
      course = Course.fromJson(json['course']);
    }
  }

  //Typically called from service or data_source layer just before persisting data.
  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map();
    data['id'] = id;
    if (quiz != null) {
      data['quiz'] = quiz.toJson();
    }
    data['type'] = type;
    data['start'] = start;
    data['end'] = end;
    data['description'] = description;
    return data;
  }

  @override
  String toString() {
    return 'Session id: $id, quiz: $quiz, start: $start, end: $end, type: $type, creator: $creator, complete: $complete, description: $description';
  }

  //return participation method as a bool
  bool getMode() {
    if (type == "anonym") {
      return true;
    } else {
      return false;
    }
  }

  //return given timestamp as a formatted string
  String getLocaleDatetime(int timestamp) {
    timestamp = timestamp * 1000;
    return DateFormat('dd.MM.yyyy HH:mm')
        .format(DateTime.fromMillisecondsSinceEpoch(timestamp));
  }
}
