import 'package:http/http.dart' as http;
import 'dart:convert';
import 'dart:typed_data';
import 'package:flutter/widgets.dart';

import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/domain/exceptions/index.dart';
import 'package:fbs_app/service/interfaces/i_api.dart';

/// Implements the IApi class from the interface folder of the service layer.
class Api implements IApi {
  static const endpoint = 'https://feedback.mni.thm.de/feedbackApp/api';

  /// The HTTP client handling Requests for a User.
  final client = new http.Client();

  dynamic _decode(String string) {
    return json.decode(utf8.decode(string.runes.toList()));
  }

  Future<http.Response> _request(Function request, String url,
      {Map<String, String> headers, body}) async {
    try {
      if (body == null) {
        // no body, no encoding.
        return await request(url, headers: headers)
            .timeout(const Duration(seconds: 20));
      } else {
        return await request(url, headers: headers, body: body, encoding: utf8)
            .timeout(const Duration(seconds: 20));
      }
    } catch (_) {
      throw NetworkErrorException.msg(
          "Etwas ist schief gelaufen. Bitte überprüfe deine Internetverbindung.");
    }
  }

  /* ************* LOG-IN & -OUT ****************** */

  /// Returns a authorization token for the [username] and [password].
  ///
  /// It is called to login and returns the authorization token used authorization in the app.
  ///
  /// When [response.statusCode] is anything else than 200 and 401 an [NetworkErrorException] is thrown.
  /// If the responses status code is 401 the username or password is invalid the method throws an [InvalidCredentialsException].
  @override
  Future<User> getAuthToken(String username, String password) async {
    const url = '$endpoint/auth';
    final headers = {"Content-Type": "application/json"};
    final body = json.encode({"username": username, "password": password});
    final response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        final res = _decode(response.body);
        final user = User.fromJson(res['user']);
        user.authToken = Token.fromJson(res);
        return user;
      case 401:
        throw InvalidCredentialsException();
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<void> invalidateAuthToken(Token authToken) async {
    const url = '$endpoint/auth';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    final response = await _request(client.delete, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return;
      case 401:
        throw InvalidCredentialsException();
      default:
        throw NetworkErrorException();
    }
  }

  /* ************* QUIZZE ***************** */

  /// Returns a session token for the [{session.id}].
  ///
  /// It is called to join a session and returns the session token used for (anonymous) authorization in the quiz.
  /// If [mode] is true the user is anonymous and [authToken] doesn't need to be provided.
  /// Otherwise it MUST or it will raise an [UnauthorizedException].
  /// When [response.statusCode] is anything else than 200 and 404 an [NetworkErrorException] is thrown.
  /// If it is a 404 a [NotFoundException] is thrown.
  @override
  Future<Session> getSessionToken(Session session, bool mode,
      [Token authToken]) async {
    var url = '$endpoint/sessions/${session.id}/join';
    var headers = {"Content-Type": "application/json"};
    if (!mode) {
      var token = authToken == null
          ? throw UnauthorizedException.msg(
          "Du musst für diese Session angemeldet sein.")
          : authToken.token;
      headers["Authorization"] = "Bearer $token";
    }
    var body = json.encode({"anonym": mode});
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        session.sessToken = Token.fromJson(_decode(response.body));
        var begin = _decode(response.body)['continue'];
        if (begin != null) session.beginAtQuestion = begin;
        return session;
      case 403:
        switch (_decode(response.body)['description']) {
          case 'you need to be a participant to join':
            throw UnauthorizedException.msg(
                'Du bist kein Teilnehmer in dem zur Session gehörenden Kurs.');
          case 'session has already ended':
            throw UnauthorizedException.msg(
                'Die Session ist zur Zeit nicht geöffnet.');
        }
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session.id}');
      case 409:
        throw UnauthorizedException.msg(
            'Diese Session wurde bereits vollständig beantwortet.');
      default:
        throw NetworkErrorException();
    }
  }

  /// Returns a [Session] with some information about a quiz with the given [{session.id}].
  ///
  /// The information is used the start screen of a quiz if somebody tries to join this quiz.
  /// When [response.statusCode] is a 404 a [NotFoundException] is thrown. Otherwise a [NetworkErrorException].
  @override
  Future<Session> getQuizInfo(int sessionId) async {
    var url = '$endpoint/sessions/$sessionId';
    var response = await _request(client.get, url);
    switch (response.statusCode) {
      case 200:
        return Session.fromJson(_decode(response.body));
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id $sessionId');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<Question>> getSessionQuestions(Session session, Token sessToken) async {
    var url = '$endpoint/sessions/${session.id}/questions';
    var token =
        sessToken == null ? throw UnauthorizedException() : sessToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> questionsFromJson = _decode(response.body);
        var list = questionsFromJson.map((e) => Question.fromJson(e)).toList();
        return list;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Question> getSessionQuestion(Session session, int questionNumber,
      Token sessToken) async {
    var url = '$endpoint/sessions/${session.id}/$questionNumber';
    var token =
    sessToken == null ? throw UnauthorizedException() : sessToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return Question.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die Frage Nr. $questionNumber in der Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<bool> submitAnswer(Session session, int questionNumber,
      Token sessToken, List<Answer> answers) async {
    var url = '$endpoint/sessions/${session.id}/questions/$questionNumber';
    var token =
    sessToken == null ? throw UnauthorizedException() : sessToken.token;
    var headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    var body = json.encode(answers.map((e) => e.toJson()).toList());
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return true; //_decode((response.body))['correct'];
      case 403:
        throw UnauthorizedException.msg('Diese Session ist abgelaufen.');
      case 404:
        throw NotFoundException.msg(
            'Die Frage Nr. $questionNumber in der Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  /* ************* STATISTIK ***************** */

  @override
  Future<Statistics> getUserSessionStatistics(Session session,
      Token sessToken) async {
    var url = '$endpoint/sessions/${session.id}/myStatistics';
    var token =
    sessToken == null ? throw UnauthorizedException() : sessToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return Statistics.fromJson(_decode(response.body));
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Statistics> getUserSessionStatisticsAuth(Session session,
      Token authToken) async {
    var url = '$endpoint/sessions/${session.id}/myStatistics/user';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return Statistics.fromJson(_decode(response.body));
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<SessionStatisticsOverview> getSessionStatisticsOverview(Session session, Token authToken) async {
    var url = '$endpoint/sessions/${session.id}/statistics';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
    };
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return SessionStatisticsOverview.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<MemoryImage> getSessionStatisticsImage(Session session,
      Token authToken) async {
    var url = '$endpoint/charts/session/${session.id}';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token", "accept": "image/png"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return MemoryImage(Uint8List.fromList(response.body.codeUnits));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die  Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<MemoryImage> getCourseStatisticsImage(Course course,
      Token authToken) async {
    var url = '$endpoint/charts/course/${course.id}';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token", "accept": "image/png"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return MemoryImage(Uint8List.fromList(response.body.codeUnits));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id ${course.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<QuestionStatistic>> getQuestionStatistics(Session session,
      Token authToken) async {
    final url = '$endpoint/sessions/${session.id}/resultStatistics';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> statsFromJson = _decode(response.body);
        var list =
        statsFromJson.map((e) => QuestionStatistic.fromJson(e)).toList();
        return list;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die  Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<MemoryImage> getWordcloudImage(Session session, int questionNumber,
      Token authToken) async {
    var url = '$endpoint/charts/wordCloud/${session.id}/$questionNumber';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token", "accept": "image/png"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return MemoryImage(Uint8List.fromList(response.body.codeUnits));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session
                .id} oder die Frage Nr. $questionNumber');
      default:
        throw NetworkErrorException();
    }
  }

  /* ************* QUIZZE Verwalten ***************** */

  @override
  Future<Session> startSession(Quiz quiz, Session session,
      Token authToken) async {
    var url = '$endpoint/quizzes/${quiz.id}/sessions';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode(session);

    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Session.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Das Quiz mit der Quiz-Id ${quiz.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Session> editSession(Session session, Token authToken) async {
    var url = "$endpoint/sessions/${session.id}";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode(session);
    var response =
    await _request(client.put, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Session.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<void> deleteSession(Session session, Token authToken) async {
    var url = "$endpoint/sessions/${session.id}";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};

    var response = await _request(client.delete, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Die Session mit der Session-Id ${session.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<Session>> getSessionsForQuiz(Quiz quiz, Token authToken) async {
    var url = "$endpoint/quizzes/${quiz.id}/sessions";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};

    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> sessionsFromJson = _decode(response.body);
        var list = sessionsFromJson.map((e) => Session.fromJson(e)).toList();
        return list;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Das Quiz mit der Quiz-Id ${quiz.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Quiz> submitQuiz(Quiz quiz, Token authToken) async {
    var url = '$endpoint/quizzes';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    var body = json.encode(quiz);
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Quiz.fromJson(_decode(response.body));
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Quiz> editQuiz(Quiz quiz, Token authToken) async {
    var url = "$endpoint/quizzes/${quiz.id}";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Content-Type": "application/json",
      "Authorization": "Bearer $token"
    };
    var body = json.encode(quiz);
    var response =
    await _request(client.put, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Quiz.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Das Quiz mit der Quiz-Id ${quiz.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<void> deleteQuiz(Quiz quiz, Token authToken) async {
    var url = "$endpoint/quizzes/${quiz.id}";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.delete, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Das Quiz mit der Quiz-Id ${quiz.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Question> submitQuestion(Quiz quiz, Question question,
      Token authToken) async {
    var url = '$endpoint/quizzes/${quiz.id}/questions';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode(question);
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Question.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Das Quiz mit der Quiz-Id ${quiz.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Question> editQuestion(Quiz quiz, Question question,
      Token authToken) async {
    var questionNumber = question.number;
    var url = "$endpoint/quizzes/${quiz.id}/questions/$questionNumber";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode(question);
    var response =
    await _request(client.put, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Question.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Das Quiz mit der Quiz-Id ${quiz.id} oder die Frage mit der Frage Nr. $questionNumber');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<void> deleteQuestion(Quiz quiz, Question question,
      Token authToken) async {
    var questionNumber = question.number;
    var url = "$endpoint/quizzes/${quiz.id}/questions/$questionNumber";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.delete, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Das Quiz mit der Quiz-Id ${quiz.id} oder die Frage mit der Frage Nr. $questionNumber');
      default:
        throw NetworkErrorException();
    }
  }

  /// Returns a List of [Question] for the quiz with [{uizI.i}].
  ///
  /// It is called to get all the questions of a quiz for the owner of the quiz, not for answering the quiz.
  /// You must call it with an valid authorization token.
  /// If the token is null it will raise an [UnauthorizedException].
  /// When [response.statusCode] is anything else than 200 an [Exception] is thrown.
  @override
  Future<List<Question>> getQuestions(int quizId, Token authToken) async {
    var url = '$endpoint/quizzes/$quizId/questions';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> questionsFromJson = _decode(response.body);
        var list = questionsFromJson.map((e) => Question.fromJson(e)).toList();
        return list;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Das Quiz mit der Quiz-Id $quizId');
      default:
        throw NetworkErrorException();
    }
  }

  /* ************* KURSE ****************** */

  @override
  Future<List<Course>> getAllCourses(Token authToken, bool hideJoined,
      [String search, int perPage, int page]) async {
    // api/courses?search=blablabla&perPage=50&page=0
    var url = hideJoined == true
        ? "$endpoint/courses?hideJoined=true"
        : "$endpoint/courses";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> coursesFromJson = _decode(response.body);
        var list = coursesFromJson.map((e) => Course.fromJson(e)).toList();
        return list;
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Course> getCourseInfo(int courseId, Token authToken) async {
    var url = '$endpoint/courses/$courseId';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return Course.fromJson(_decode(response.body));
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Course> joinCourse(int courseId, Token authToken,
      [int password]) async {
    var url = '$endpoint/courses/$courseId/join';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = password == null
        ? json.encode({})
        : json.encode({"password": password});
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Course.fromJson(_decode(response.body));
      case 401:
        throw InvalidCredentialsException.msg("Ungültiges Passwort.");
      case 403:
        throw InvalidCredentialsException.msg("Ungültiges Passwort.");
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      case 409:
        throw UnauthorizedException.msg('Bereits Mitgleid in diesem Kurs.');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<void> leaveCourse(int courseId, Token authToken) async {
    var url = '$endpoint/courses/$courseId/leave';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
    };
    var response = await _request(client.post, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return;
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Course> submitCourse(Course course, Token authToken) async {
    var url = "$endpoint/courses";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode(course);
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Course.fromJson(_decode(response.body));
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Course> editCourse(Course course, Token authToken) async {
    var url = "$endpoint/courses/${course.id}";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode(course);
    var response =
    await _request(client.put, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Course.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id ${course.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<void> deleteCourse(Course course, Token authToken) async {
    var url = "$endpoint/courses/${course.id}";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.delete, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id ${course.id}');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<User>> getCourseAdmins(int courseId, Token authToken) async {
    var url = '$endpoint/courses/$courseId/admins';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> adminsFromJson = _decode(response.body);
        var list = adminsFromJson.map((e) => User.fromJson(e)).toList();
        return list;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<User> submitCourseAdmin(int courseId, User user,
      Token authToken) async {
    var url = "$endpoint/courses/$courseId/admins";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode({"username": user.username});
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return User.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      case 409:
        throw UnauthorizedException.msg('Benutzer ist bereits Kurs-Admin.');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<User> deleteCourseAdmin(int courseId, User user,
      Token authToken) async {
    var url = "$endpoint/courses/$courseId/admins/${user.id}";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.delete, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return User.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<Session> submitCourseSession(Course course, int sessionId,
      Token authToken) async {
    var url = "$endpoint/courses/${course.id}/sessions";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
      "Content-Type": "application/json"
    };
    var body = json.encode({"sessionID": sessionId});
    var response =
    await _request(client.post, url, headers: headers, body: body);
    switch (response.statusCode) {
      case 200:
        return Session.fromJson(_decode(response.body));
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg(
            'Der Kurs mit der Kurs-Id ${course.id} oder die Session mit de Session-Id $sessionId');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<Session>> getSessionsForCourse(int courseId,
      Token authToken) async {
    var url = '$endpoint/courses/$courseId/sessions';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> sessionsFromJson = _decode(response.body);
        var list = sessionsFromJson.map((e) => Session.fromJson(e)).toList();
        return list;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<User>> getCourseParticipants(int courseId, Token authToken,
      {bool hideAdmins = false}) async {
    var url = '$endpoint/courses/$courseId/participants?hideAdmins=$hideAdmins';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> usersFromJson = _decode(response.body);
        var list = usersFromJson.map((e) => User.fromJson(e)).toList();
        return list;
      case 403:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      default:
        throw NetworkErrorException();
    }
  }

  /* ************* BENUTZER ****************** */

  @override
  Future<List<Course>> getCoursesForUser(Token authToken,
      {String role = 'all'}) async {
    var url = '$endpoint/users/me/courses?role=$role';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> coursesFromJson = _decode(response.body);
        var list =
        coursesFromJson.map((e) => Course.fromJsonForUser(e)).toList();
        return list;
      case 401:
        throw UnauthorizedException();
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<Session>> getCreatedSessionsForUser(Token authToken,
      {bool hideEnded = false, bool hideNonStarted = false}) async {
    var url =
        "$endpoint/sessions?hideEnded=$hideEnded&hideNonStarted=$hideNonStarted";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> sessionsFromJson = _decode(response.body);
        var list = sessionsFromJson.map((e) => Session.fromJson(e)).toList();
        return list;
      case 404:
        throw NotFoundException();
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<Session>> getDoneSessionsForUser(Token authToken) async {
    var url = '$endpoint/users/me/sessions';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> sessionsFromJson = _decode(response.body);
        var list = sessionsFromJson.map((e) => Session.fromJson(e)).toList();
        return list;
      case 401:
        throw UnauthorizedException();
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<Session>> getUndoneSessionsForUser(Token authToken) async {
    var url = '$endpoint/users/me/todoSessions';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> sessionsFromJson = _decode(response.body);
        var list = sessionsFromJson.map((e) => Session.fromJson(e)).toList();
        return list;
      case 401:
        throw UnauthorizedException();
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<List<Quiz>> getQuizzesForUser(Token authToken) async {
    var url = '$endpoint/quizzes';
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {
      "Authorization": "Bearer $token",
    };
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        List<dynamic> quizzesFromJson = _decode(response.body);
        var list = quizzesFromJson.map((e) => Quiz.fromJson(e)).toList();
        return list;
      case 401:
        throw UnauthorizedException();
      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<User> getUserProfile(Token authToken) async {
    var url = "$endpoint/users/me";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return User.fromJson(_decode(response.body));
      case 401:
        if (_decode(response.body)['description'] == "Revoked bearer token") {
          throw UnauthorizedException.msg(
              'Anmeldeperiode ist abgelaufen. Bitte melde dich erneut an.');
        }
        throw UnauthorizedException();

      default:
        throw NetworkErrorException();
    }
  }

  @override
  Future<UserStatistics> getUserStats(Token authToken) async {
    var url = "$endpoint/users/me/stats";
    var token =
    authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return UserStatistics.fromJson(_decode(response.body));
      case 401:
        throw UnauthorizedException();
      default:
        throw NetworkErrorException();
    }
  }

  Future<UserStatistics> getUserCourseStats(
      Token authToken, int courseId) async {
    var url = "$endpoint/courses/$courseId/stats";
    var token =
        authToken == null ? throw UnauthorizedException() : authToken.token;
    var headers = {"Authorization": "Bearer $token"};
    var response = await _request(client.get, url, headers: headers);
    switch (response.statusCode) {
      case 200:
        return UserStatistics.fromJson(_decode(response.body));
      case 401:
        throw UnauthorizedException();
      case 404:
        throw NotFoundException.msg('Der Kurs mit der Kurs-Id $courseId');
      default:
        throw NetworkErrorException();
    }
  }
}
