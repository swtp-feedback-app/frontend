import 'package:fbs_app/domain/exceptions/index.dart';
import 'package:fbs_app/service/exceptions/input_exception.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';

class ErrorHandler {
  static String errorMessage(dynamic error) {
    if (error == null) {
      return null;
    } else if (error is NetworkErrorException) {
      return error.message;
    } else if (error is NotFoundException) {
      return error.message;
    } else if (error is UnauthorizedException) {
      return error.message;
    } else if (error is InvalidCredentialsException) {
      return error.message;
    } else if (error is ValidationException) {
      return error.message;
    } else if (error is InvalidCredentialsInputException) {
      return error.message;
    } else if (error is InvalidSessionIdInputException) {
      return error.message;
    }

    print(error); // Fehler auf Konsole loggen
    return "Ein unerwarteter Fehler ist aufgetreten."; // Möglichkeit einen unbekannten Fehler zu melden, App bleibt funktionsfähig.
  }

  static void showErrorDialog(BuildContext context, dynamic error) {
    if (error == null) {
      return;
    }
    showDialog(
      context: context,
      builder: (context) {
        return AlertDialog(
            title: Text("Fehler", style: TextStyle(color: font_dark)),
            content: Text(error.message));
      },
    );
  }
}
