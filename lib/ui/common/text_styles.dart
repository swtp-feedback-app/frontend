import 'package:flutter/material.dart';
import 'app_colors.dart';

const headerStyle = TextStyle(fontSize: 35, fontWeight: FontWeight.w900);
const subHeaderStyle = TextStyle(fontSize: 16.0, fontWeight: FontWeight.w500);

const headline2 =
    TextStyle(fontSize: 32, fontWeight: FontWeight.w400, color: font_dark);

const headline3 =
    TextStyle(fontSize: 26, fontWeight: FontWeight.w400, color: font_dark);

const headline4 =
    TextStyle(fontSize: 20, fontWeight: FontWeight.bold, color: font_dark);

/* Used for List headlines */
const headline5 =
    TextStyle(fontSize: 16, fontWeight: FontWeight.bold, color: font_dark);
const headline5Web =
    TextStyle(fontSize: 18, fontWeight: FontWeight.bold, color: font_dark);

/* Used for List content */
const headline6 = TextStyle(fontSize: 12, color: font_dark);
const headline6Web = TextStyle(fontSize: 16, color: font_dark);
