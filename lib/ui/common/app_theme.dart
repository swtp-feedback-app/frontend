import 'package:flutter/material.dart';
import 'app_colors.dart';

const greenHighlight = Color(0xFFDFEEC8);
const yellowHighlight = Color(0xFFF9D47F);
const redHighlight = Color(0xFFB54E62);
const darkRedHighlight = Color(0xFF9C132E);
const blueHighlight = Color(0xFF405E9A);
const greyBackground = Color(0xFFF2F4F5);
const greyLetters = Color(0xFF394A59);
const THMGreen = Color(0xFF80BA24);
const THMGrey = Color(0xFF4A5C66);

final text = textTheme();
final theme = appTheme();

ThemeData appTheme() {
  return ThemeData(
    primaryColor: THMGreen,
    // used for input fields
    primaryColorLight: greenHighlight,
    primaryColorDark: THMGrey,
    highlightColor: THMGreen,
    backgroundColor: greyBackground,
    accentColor: greenHighlight,
    hintColor: input_grey,
    errorColor: darkRedHighlight,
    focusColor: redHighlight,
    buttonColor: THMGrey,
    bottomAppBarColor: Colors.white,
    primaryTextTheme: textTheme(),
  );
}

TextTheme textTheme() {
  return TextTheme(
      headline1: TextStyle(),
      headline2: TextStyle(),
      /* Used for List headlines */
      headline5: TextStyle(
          fontSize: 16, fontWeight: FontWeight.bold, color: greyLetters),
      /* Used for List content */
      headline6: TextStyle(fontSize: 12, color: greyLetters));
}
