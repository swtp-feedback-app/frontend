import 'package:fbs_app/ui/pages/courses/courses_detail/course_detail_statistics/course_detail_statistics.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail/courses_detail_page.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail_admin/courses_detail_admin_page.dart';
import 'package:fbs_app/ui/pages/courses/courses_findCourses/courses_findCourses_page.dart';
import 'package:fbs_app/ui/pages/courses/courses_list/courses_list_page.dart';
import 'package:fbs_app/ui/pages/courses/create_course/course_edit.dart';
import 'package:fbs_app/ui/pages/courses/create_course/create_new_course.dart';
import 'package:fbs_app/ui/pages/home_page/home_student_page.dart';
import 'package:fbs_app/ui/pages/impressum/impressum_page.dart';
import 'package:fbs_app/ui/pages/me/me_courses/me_courses_list_page.dart';
import 'package:fbs_app/ui/pages/me/me_courses/me_courses_students/me_courses_students_list_page.dart';
import 'package:fbs_app/ui/pages/me/me_quizzes/quizzes_list_page.dart';
import 'package:fbs_app/ui/pages/me/me_sessions/me_sessions_detail/me_session_detail_page.dart';
import 'package:fbs_app/ui/pages/me/me_sessions/sessions_list_page.dart';
import 'package:fbs_app/ui/pages/more/add_session_to_quiz.dart';
import 'package:fbs_app/ui/pages/more/contact.dart';
import 'package:fbs_app/ui/pages/more/more_page.dart';
import 'package:fbs_app/ui/pages/more/profile.dart';
import 'package:fbs_app/ui/pages/more/session_edit.dart';
import 'package:fbs_app/ui/pages/quizzes/new_quiz/new_quiz_start.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_begin/quiz_active.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_end/quiz_end_page.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_begin/quiz_begin_page.dart';
import 'package:fbs_app/ui/pages/quizzes/new_quiz/new_quiz.dart';
import 'package:fbs_app/ui/pages/statistics/course_stats.dart';
import 'package:fbs_app/ui/pages/statistics/session_stats.dart';
import 'package:fbs_app/ui/pages/statistics/statistics_questions/statistics_questions_detail/statistics_questions_detail_page.dart';
import 'package:fbs_app/ui/pages/statistics/statistics_questions/statistics_questions_list_page.dart';
import 'package:fbs_app/ui/pages/statistics/student_stats.dart';
import 'package:flutter/material.dart';
import 'package:page_transition/page_transition.dart';
import 'pages/quizzes/quiz_detail/quiz_detail_page.dart';
import 'pages/home_page/home_page.dart';
import 'pages/login_page/login_page.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail/course_detail_participants/course_participants_page.dart';

class Router {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case '/':
        return MaterialPageRoute(builder: (_) => HomePage());
      case 'login':
        return MaterialPageRoute(builder: (_) => LoginPage());
      case 'home':
        return MaterialPageRoute(builder: (_) => HomePage());
      case 'home-student':
        return MaterialPageRoute(builder: (_) => HomeStudentPage());
      case 'more':
        return MaterialPageRoute(builder: (_) => MorePage());
      case 'contact':
        return MaterialPageRoute(builder: (_) => ContactPage());
      case 'me/quizzes':
        return MaterialPageRoute(builder: (_) => QuizzesListPage());
      case 'me/quizzes/detail':
        return MaterialPageRoute(
            builder: (_) => QuizDetailPage(quiz: settings.arguments));
      case 'me/sessions':
        return MaterialPageRoute(builder: (_) => SessionsListPage());
      case 'me/sessions/detail':
        return MaterialPageRoute(
            builder: (_) => SessionDetailPage(session: settings.arguments));
      case 'me/courses':
        return MaterialPageRoute(builder: (_) => MeCoursesListPage());
      case 'me/courses/detail':
        return MaterialPageRoute(
            builder: (_) => CoursesDetailAdminPage(
                  course: settings.arguments,
                ));
      case 'courses/detail/students':
        return MaterialPageRoute(
            builder: (_) => MeCoursesStudentsListPage(
                  course: settings.arguments,
                ));
      case 'courses/detail/statistics':
        return MaterialPageRoute(
            builder: (_) => CourseDetailStatisticsPage(
                  course: settings.arguments,
                ));
      case 'courses':
        return MaterialPageRoute(builder: (_) => CoursesListPage());
      case 'courses/findCourse':
        return PageTransition(
            child: CoursesFindCoursesPage(), type: PageTransitionType.downToUp);
      case 'courses/detail':
        return MaterialPageRoute(
            builder: (_) => CoursesDetailPage(
                  course: settings.arguments,
                ));
      case 'courses/detail/admins':
        return MaterialPageRoute(
            builder: (_) => CoursesParticipantsPage(
                  admin: true,
                  course: settings.arguments,
                ));
      case 'courses/detail/participants':
        return PageTransition(
            child: CoursesParticipantsPage(
              admin: false,
              course: settings.arguments,
            ),
            type: PageTransitionType.downToUp);
      case 'courses/create':
        return MaterialPageRoute(builder: (_) => CreateCoursePage());
      case 'quiz/end':
        return MaterialPageRoute(
            builder: (_) => QuizEndPage(settings.arguments));
      case 'quiz/begin':
        return MaterialPageRoute(
            builder: (_) => QuizStartPage(settings.arguments));
      case 'quiz/newquiz':
        return MaterialPageRoute(
            builder: (_) => NewQuizStart(settings.arguments));
      case 'quiz/edit':
        return MaterialPageRoute(
            builder: (_) => NewQuestion(settings.arguments));
      case 'quiz/active':
        return MaterialPageRoute(
            builder: (_) => QuizActivePage(settings.arguments));
      case 'impressum':
        return MaterialPageRoute(builder: (_) => Impressum());
      case 'statistics/session':
        return MaterialPageRoute(
            builder: (_) => SessionStatsPage(
                  session: settings.arguments,
                ));
      case 'statistics/questions':
        return MaterialPageRoute(
            builder: (_) => StatisticsQuestionListPage(
                  session: settings.arguments,
                ));
      case 'statistics/questions/detail':
        return MaterialPageRoute(
            builder: (_) => StatisticsQuestionsDetailPage(
                  index: settings.arguments,
                ));
      case 'statistics/user':
        return MaterialPageRoute(builder: (_) => StudentCheck());
      case 'statistics/course':
        return MaterialPageRoute(
            builder: (_) => CourseStatsPage(course: settings.arguments));
      case 'me/profile':
        return MaterialPageRoute(builder: (_) => MeProfilePage());
      case 'quiz/newsession':
        return MaterialPageRoute(
            builder: (_) => CreateNewSessionPage(settings.arguments));
      case 'session/edit':
        return MaterialPageRoute(
            builder: (_) => SessionEditPage(settings.arguments));
      case 'me/courses/edit':
        return MaterialPageRoute(
            builder: (_) => EditCoursePage(settings.arguments));
      default:
        return MaterialPageRoute(builder: (_) {
          return Scaffold(
            body: Center(
              child: Text('No route defined for ${settings.name}'),
            ),
          );
        });
    }
  }
}
