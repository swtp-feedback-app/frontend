import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'fbs_bottom_navbar.dart';

class WebNavigation extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final Widget body;
  final String selectedRoute;

  WebNavigation(this.body, this.selectedRoute);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Drawer(
            child: ListView(
          padding: EdgeInsets.zero,
          children: <Widget>[
            Container(
              height: 100,
              child: DrawerHeader(
                child: Container(),
                decoration: BoxDecoration(
                  color: bg_dark,
                ),
              ),
            ),
            FlatButton(
              highlightColor: Colors.transparent,
              onPressed: () {
                Navigator.pushNamed(context, "home");
              },
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: bg_dark),
                )),
                child: Row(children: [
                  Icon(
                    Icons.home,
                    color: selectedRoute == '/' ? THMGreen : greyLetters,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Home",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
            ),
            FlatButton(
              highlightColor: Colors.transparent,
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: bg_dark),
                )),
                child: Row(children: [
                  Icon(Icons.collections_bookmark,
                      color:
                          selectedRoute == 'courses' ? THMGreen : greyLetters),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Kurse",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
              onPressed: () {
                Navigator.pushNamed(context, "courses");
              },
            ),
            FlatButton(
              highlightColor: Colors.transparent,
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: bg_dark),
                )),
                child: Row(children: [
                  Icon(Icons.person,
                      color:
                          selectedRoute == 'profile' ? THMGreen : greyLetters),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Profil",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
              onPressed: () {
                Navigator.pushNamed(context, "me/profile");
              },
            ),
            (user.docent)
                ? FlatButton(
                    highlightColor: Colors.transparent,
                    child: Container(
                      padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                      decoration: BoxDecoration(
                          border: Border(
                        bottom: BorderSide(width: 1.0, color: bg_dark),
                      )),
                      child: Row(children: [
                        Icon(
                          Icons.school,
                          color: selectedRoute == 'me/courses'
                              ? THMGreen
                              : greyLetters,
                        ),
                        SizedBox(
                          width: 8,
                        ),
                        Text(
                          "Meine Kurse verwalten",
                          style: TextStyle(
                            fontSize: 18,
                          ),
                        ),
                      ]),
                    ),
                    onPressed: () {
                      Navigator.pushNamed(context, "me/courses");
                    },
                  )
                : Container(),
            FlatButton(
              highlightColor: Colors.transparent,
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: bg_dark),
                )),
                child: Row(children: [
                  Icon(
                    Icons.question_answer,
                    color:
                        selectedRoute == 'me/quizzes' ? THMGreen : greyLetters,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Meine Quizze verwalten",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
              onPressed: () {
                Navigator.pushNamed(context, "me/quizzes");
              },
            ),
            FlatButton(
              highlightColor: Colors.transparent,
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: bg_dark),
                )),
                child: Row(children: [
                  Icon(Icons.schedule,
                      color: selectedRoute == 'me/sessions'
                          ? THMGreen
                          : greyLetters),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Meine Sessions verwalten",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
              onPressed: () {
                Navigator.pushNamed(context, "me/sessions");
              },
            ),
            FlatButton(
              highlightColor: Colors.transparent,
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: bg_dark),
                )),
                child: Row(children: [
                  Icon(
                    Icons.create,
                    color: selectedRoute == 'quiz/newquiz'
                        ? THMGreen
                        : greyLetters,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Quiz erstellen",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
              onPressed: () {
                Navigator.pushNamed(context, "quiz/newquiz");
              },
            ),
            FlatButton(
              highlightColor: Colors.transparent,
              child: Container(
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                decoration: BoxDecoration(
                    border: Border(
                  bottom: BorderSide(width: 1.0, color: bg_dark),
                )),
                child: Row(children: [
                  Icon(
                    Icons.info,
                    color: bg_dark,
                  ),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Impressum",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
              onPressed: () {
                Navigator.pushNamed(context, "impressum");
              },
            ),
            FlatButton(
              highlightColor: Colors.transparent,
              child: Container(
                decoration: BoxDecoration(
                    color: white,
                    border: Border(
                      bottom: BorderSide(width: 1.0, color: bg_dark),
                    )),
                padding: EdgeInsets.fromLTRB(0, 25, 25, 25),
                child: Row(children: [
                  Icon(Icons.mail, color: greyLetters),
                  SizedBox(
                    width: 8,
                  ),
                  Text(
                    "Kontakt",
                    style: TextStyle(
                      fontSize: 18,
                    ),
                  ),
                ]),
              ),
              onPressed: () {
                Navigator.pushNamed(context, "contact");
              },
            ),
          ],
        )),
        Expanded(
          child: Scaffold(
            body: body,
            bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                ? FbsBottomNavbar(index: 1)
                : null,
          ),
        )
      ],
    );
  }

  showAlertDialog(BuildContext context) {
    final TextEditingController sessionIdController =
        new TextEditingController();
    Widget cancel = FlatButton(
      child: Text("Abbrechen", style: TextStyle(color: font_dark)),
      onPressed: () {
        Navigator.of(context).pop();
      },
    );

    Widget ok = WhenRebuilderOr<SessionService>(
      observe: () => RM.get<SessionService>(),
      onWaiting: () => Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(green),
        ),
      ),
      dispose: (_, __) {
        sessionIdController.dispose();
      },
      builder: (_, sessServiceRM) {
        return Center(
          child: FlatButton(
            child: Text("OK", style: TextStyle(color: font_dark)),
            onPressed: () {
              sessServiceRM.setState(
                  (state) => state.getSession(sessionIdController.text),
                  onData: (context, sessServiceRM) {
                if (sessServiceRM.session != null) {
                  Navigator.pushNamed(context, 'quiz/begin',
                      arguments: sessServiceRM.session);
                }
              }, onError: (context, e) {
                ErrorHandler.showErrorDialog(context, e);
              });
            },
          ),
        );
      },
    );
    // set up the AlertDialog
    AlertDialog alert = AlertDialog(
      title: Text("Quiz-Id eingeben", style: TextStyle(color: font_dark)),
      content: TextField(
        controller: sessionIdController,
        decoration: InputDecoration(
          hintText: 'Beispiel: 12345678',
          border: new OutlineInputBorder(
              borderRadius: new BorderRadius.circular(30.0),
              borderSide: new BorderSide()),
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: green, width: 2.0),
            borderRadius: BorderRadius.circular(30.0),
          ),
        ),
      ),
      actions: [
        cancel,
        ok,
      ],
    );
    // show the dialog
    showDialog(
      context: context,
      builder: (BuildContext context) {
        return alert;
      },
    );
  }
}
