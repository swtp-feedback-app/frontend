import 'package:flutter/material.dart';
import '../common/app_colors.dart';

class MainAppLayout extends StatelessWidget {
  MainAppLayout({this.headerHeight});

  final double headerHeight;

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          height: headerHeight,
          decoration: BoxDecoration(color: bg_green),
        ),
        Expanded(
          child: Container(
            decoration: BoxDecoration(color: bg_light),
          ),
        ),
      ],
    );
  }
}
