import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:flutter/material.dart';

class FBSAppBar extends StatelessWidget implements PreferredSizeWidget {
  final double height;
  final String title;
  final Widget leading;
  final Widget action;

  const FBSAppBar({
    this.height = 65,
    this.leading,
    this.title,
    this.action,
  });

  @override
  Widget build(BuildContext context) {
    return AppBar(
      title: Container(
          padding: EdgeInsets.symmetric(horizontal: 8),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              leading ?? backButton(context),
              Container(
                  width: MediaQuery.of(context).size.width > 768
                      ? MediaQuery.of(context).size.width - 160 - 350
                      : MediaQuery.of(context).size.width -
                          160, //350 for drawer
                  child: Text(
                    title,
                    style: headline3,
                    textAlign: TextAlign.center,
                  )),
              action ?? SizedBox(width: 40),
            ],
          )),
      backgroundColor: bg_green,
      automaticallyImplyLeading: false,
      elevation: 0,
    );
  }

  Widget backButton(BuildContext context) {
    return InkWell(
      highlightColor: Colors.transparent,
      onTap: () {
        Navigator.pop(context);
      },
      child: CircleAvatar(
        backgroundColor: bg_dark,
        radius: 20,
        child: Icon(Icons.arrow_back, color: Colors.white),
      ),
    );
  }

  @override
  Size get preferredSize => Size.fromHeight(height);
}
