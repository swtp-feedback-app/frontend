import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';

class FbsSearchField extends StatelessWidget {
  FbsSearchField({@required this.onChanged, @required this.textController});

  final Function onChanged;
  final TextEditingController textController;

  @override
  Widget build(BuildContext context) {
    return TextField(
      onChanged: onChanged,
      cursorColor: THMGreen,
      controller: textController,
      decoration: InputDecoration(
          labelText: "Suchen",
          labelStyle: TextStyle(color: input_grey),
          hintText: "Suchen",
          hintStyle: TextStyle(color: input_grey),
          prefixIcon: Icon(Icons.search),
          focusColor: THMGreen,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(color: THMGreen, width: 2.0),
            borderRadius: BorderRadius.circular(12.0),
          ),
          border: OutlineInputBorder(
              borderRadius: BorderRadius.all(Radius.circular(12.0)))),
    );
  }
}
