import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/service/statistics_service.dart';

import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:fbs_app/ui/widgets/main_app_bg.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class StudentCheck extends StatefulWidget {
  StudentCheck({Key key}) : super(key: key);

  @override
  _StudentCheckPage createState() => _StudentCheckPage();
}

class _StudentCheckPage extends State<StudentCheck> {
  final userStats = Injector.get<UserSessionStatisticsService>().stats;
  final session = Injector.get<SessionService>().session;
  final user = Injector.get<AuthenticationService>().user;
  final course = Injector.get<SessionService>().course;

  Widget _card(BuildContext context, String main, String value) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 3,
            blurRadius: 5,
            offset: Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      width: 150,
      height: 150,
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(main,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Color(0XFF394A59))),
              Text(value,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, color: Color(0XFF394A59))),
            ],
          ),
        ),
      ),
    );
  }

  Widget _bigcard(BuildContext context, String main, String value) {
    return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(12),
        boxShadow: [
          BoxShadow(
            color: Colors.grey.withOpacity(0.1),
            spreadRadius: 3,
            blurRadius: 5,
            offset: Offset(0, 5), // changes position of shadow
          ),
        ],
      ),
      //  width: 150,
      constraints: BoxConstraints(maxWidth: 300, maxHeight: 150),
      child: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            children: <Widget>[
              Text(main,
                  textAlign: TextAlign.center,
                  style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.bold,
                      color: Color(0XFF394A59))),
              Text(value,
                  textAlign: TextAlign.center,
                  style: TextStyle(fontSize: 20, color: Color(0XFF394A59))),
            ],
          ),
        ),
      ),
    );
  }

  Widget build(BuildContext context) {
    String name = user == null ? "anonymen Nutzer" : user.username;
    final screenSize = MediaQuery.of(context).size;
    final _scrollController = ScrollController();
    return Scaffold(
        body: Stack(children: [
      Container(
        height: screenSize.height,
        width: screenSize.width,
        child: MainAppBackground(headerHeight: screenSize.height / 3),
      ),
      SafeArea(
          child: Center(
              child: Container(
                  padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
                  constraints: (screenSize.width > 768)
                      ? BoxConstraints(minWidth: 100, maxWidth: 500)
                      : null,
                  height: screenSize.height - 50,
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        constraints: (screenSize.width > 768)
                            ? BoxConstraints(minWidth: 100, maxWidth: 500)
                            : null,
                        child: Text(
                            "Ergebnis in Quiz '" +
                                session.quiz.name +
                                "' für " +
                                name,
                            textAlign: TextAlign.left,
                            style: headline2),
                      ),
                      SizedBox(height: 30),
                      Expanded(
                        child: Scrollbar(
                          isAlwaysShown: screenSize.aspectRatio >= 0.6,
                          controller: _scrollController,
                          child: SingleChildScrollView(
                            controller: _scrollController,
                            child: Column(
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: [
                                  Wrap(
                                    children: [
                                      _card(context, "Anzahl der Fragen",
                                          userStats.questionCount.toString()),
                                      UIHelper.horizontalSpaceSmall(),
                                      _card(context, "Richtig beantwortet",
                                          userStats.correct.toString()),
                                    ],
                                  ),
                                  UIHelper.verticalSpaceMedium(),
                                  Wrap(
                                    children: [
                                      _bigcard(context, "Ergebnis insgesamt",
                                          userStats.getResult() + "%"),
                                    ],
                                  ),
                                ]),
                          ),
                        ),
                      ),
                      UIHelper.verticalSpaceMedium(),
                      Container(
                          constraints: (screenSize.width > 768)
                              ? BoxConstraints(minWidth: 100, maxWidth: 500)
                              : null,
                          child: FbsButton(
                              onPressed: () {
                                RM.get<SessionService>().refresh();
                                Navigator.pop(context);
                                if (user == null) {
                                  Navigator.pushReplacementNamed(
                                      context, 'login');
                                } else {
                                  course != null
                                      ? Navigator.pushReplacementNamed(
                                          context, 'courses/detail',
                                          arguments: course)
                                      : Navigator.pushReplacementNamed(
                                      context, 'home');
                                }
                              },
                              text: 'Beenden',
                              type: 'secondary',
                              size: 'big')),
                      UIHelper.verticalSpaceSmall()
                    ],
                  )))),
    ]));
  }
}
