import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/pages/statistics/statistics_questions/statistics_questions_list_list.dart';
import 'package:fbs_app/ui/widgets/fbs_action_button.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:fbs_app/ui/widgets/fbs_searchfield.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';

import '../../../common/app_theme.dart';

class StatisticsQuestionListPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final Session session;
  final TextEditingController searchController = new TextEditingController();

  static const routeName = 'statistics/questions';

  StatisticsQuestionListPage({this.session});

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => SessionStatisticsService(api: Injector.get()))],
        builder: (context) {
          return Scaffold(
            appBar: FBSAppBar(
                title: 'Statistiken', action: FbsActionButton(type: 'search')),
            body: Center(
              child: Container(
                constraints:
                    BoxConstraints(maxWidth: 850), // limit the width for Web
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(24, 8, 24, 0),
                  child: Column(
                    children: <Widget>[
                      WhenRebuilderOr<CoursesService>(
                        observe: () => RM.get<CoursesService>(),
                        initState: (_, sessionStatisticsServiceRM) {
                          sessionStatisticsServiceRM.setState(
                            (state) => state.search = false,
                          );
                        },
                        onWaiting: () {
                          return Container();
                        },
                        builder: (_, sessionStatisticsService) {
                          return Visibility(
                            visible: sessionStatisticsService.state.search,
                            child: Column(
                              children: <Widget>[
                                UIHelper.verticalSpaceSmall(),
                                FbsSearchField(
                                  onChanged: (value) {
                                    RM.get<SessionStatisticsService>().setState(
                                        (state) => state
                                            .getSessionQuestionsStatsFilter(
                                                value), onError: (context, e) {
                                      ErrorHandler.showErrorDialog(context, e);
                                    });
                                  },
                                  textController: searchController,
                                )
                              ],
                            ),
                          );
                        },
                      ),
                      GestureDetector(
                          onTap: () {
                            Navigator.pushNamed(context, 'statistics/session',
                                arguments: session);
                          },
                          child: FbsListCard(
                              heading: 'Gesamtübersicht',
                              subHeading: 'Alle Fragen auf einen Blick')),
                      WhenRebuilderOr<SessionStatisticsService>(
                        observe: () => RM.get<SessionStatisticsService>(),
                        onError: (error) {
                          return Expanded(
                            child: Center(
                              child: Text(ErrorHandler.errorMessage(error),
                                  textAlign: TextAlign.center),
                            ),
                          );
                        },
                        initState: (_, sessionStatisticsServiceRM) {
                          try {
                            sessionStatisticsServiceRM.setState(
                              (state) => state.getSessionQuestionStats(
                                  session, user.authToken),
                            );
                          } catch (e) {
                            ErrorHandler.showErrorDialog(context, e);
                          }
                        },
                        onWaiting: () {
                          return Expanded(
                            child: Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    new AlwaysStoppedAnimation<Color>(THMGreen),
                              ),
                            ),
                          );
                        },
                        builder: (_, sessionStatisticsService) {
                          return StatisticsQuestionsList(
                              questionStats:
                                  sessionStatisticsService.state.questionStats);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                ? FbsBottomNavbar(index: 2)
                : null,
          );
        });
  }
}
