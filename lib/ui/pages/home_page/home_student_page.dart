import 'package:fbs_app/ui/pages/home_page/home_join_quiz.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/pages/home_page/home_header.dart';
import 'package:fbs_app/ui/pages/home_page/home_recentlist.dart';
import 'package:fbs_app/ui/widgets/main_app_bg.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import '../../../service/authentication_service.dart';
import '../../../service/courses_service.dart';
import '../../exceptions/error_handler.dart';

class HomeStudentPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => CoursesService(api: Injector.get()))],
        builder: (context) {
          return GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Scaffold(
              body: WhenRebuilderOr<CoursesService>(
                observe: () => RM.get<CoursesService>(),
                initState: (_, coursesServiceRM) {
                  coursesServiceRM.setState(
                      (state) => state.getUndoneSessionsForUser(user.authToken),
                      onError: (context, e) {
                    ErrorHandler.showErrorDialog(context, e);
                  });
                },
                onWaiting: () => Center(child: CircularProgressIndicator()),
                builder: (_, coursesService) {
                  return Stack(
                    children: <Widget>[
                      MainAppBackground(headerHeight: 200),
                      SafeArea(
                        child: Center(
                          child: Container(
                            constraints: BoxConstraints(
                                maxWidth: 850), // limit the width for Web
                            child: Padding(
                              padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
                              child: Column(
                                children: <Widget>[
                                  HomeHeader(name: user.username),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  HomeJoinQuiz(user.authToken),
                                  SizedBox(
                                    height: 40,
                                  ),
                                  HomeRecentList(
                                      sessions:
                                          coursesService.state.undoneSessions),
                                ],
                              ),
                            ),
                          ),
                        ),
                      )
                    ],
                  );
                },
              ),
              bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                  ? FbsBottomNavbar(index: 1)
                  : null,
            ),
          );
        });
  }
}
