import 'package:fbs_app/domain/entities/token.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class HomeJoinQuiz extends StatelessWidget {
  HomeJoinQuiz(this.authToken);

  final Token authToken;
  final TextEditingController sessionIdController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      Expanded(
          child: Container(
              decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(12),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.1),
                      spreadRadius: 3,
                      blurRadius: 5,
                      offset: Offset(0, 5), // changes position of shadow
                    ),
                  ]),
              height: 145,
              child: WhenRebuilderOr<SessionService>(
                  observe: () => RM.get<SessionService>(),
                  onWaiting: () => Center(
                        child: CircularProgressIndicator(
                          valueColor: new AlwaysStoppedAnimation<Color>(green),
                        ),
                      ),
                  dispose: (_, __) {
                    sessionIdController.dispose();
                  },
                  builder: (_, sessServiceRM) {
                    return Row(
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          Column(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                  child: Padding(
                                    padding: EdgeInsets.fromLTRB(0, 10.0, 0, 0),
                                    child: Text('Session beitreten',
                                        textAlign: TextAlign.start,
                                        style: TextStyle(
                                            fontWeight: FontWeight.bold,
                                            fontSize: 25,
                                            color: font_dark)),
                                  ),
                                ),
                                Padding(
                                    padding: EdgeInsets.fromLTRB(0, 0, 0, 10.0),
                                    child: SizedBox(
                                      width: MediaQuery.of(context).size.width *
                                                  0.5 <
                                              200
                                          ? MediaQuery.of(context).size.width *
                                              0.5
                                          : 200,
                                      child: TextField(
                                        keyboardType: TextInputType.number,
                                        controller: sessionIdController,
                                        decoration: InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                              borderRadius:
                                                  BorderRadius.circular(5.0),
                                              borderSide: BorderSide(
                                                color: green,
                                              )),
                                          hintText: 'Beispiel: 12345678',
                                          hoverColor: green,
                                        ),
                                        onSubmitted: (String input) {
                                          if (input.isNotEmpty) {
                                            sessServiceRM.setState(
                                                (state) => state.getSession(
                                                    sessionIdController.text),
                                                //Show error msg under input
                                                onData:
                                                    (context, sessServiceRM) {
                                              if (sessServiceRM.session !=
                                                  null) {
                                                Navigator.pushNamed(
                                                    context, 'quiz/begin',
                                                    arguments:
                                                        sessServiceRM.session);
                                              }
                                            }, onError: (context, e) {
                                              ErrorHandler.showErrorDialog(
                                                  context, e);
                                            });
                                          }
                                        },
                                      ),
                                    )),
                                UIHelper.verticalSpaceSmall(),
                                WhenRebuilderOr(
                                  observe: () => RM.get<SessionService>(),
                                  /* onError: (error) =>  Text(
                                    ErrorHandler.errorMessage(error),
                                    style: TextStyle(color: Colors.red),
                                  ),*/
                                  builder: (_, __) => Container(),
                                ),
                                UIHelper.verticalSpaceSmall(),
                              ]),
                          Padding(
                              padding: EdgeInsets.fromLTRB(10.0, 0, 0, 10.0),
                              child: SizedBox(
                                  height: 100,
                                  child: InkWell(
                                    onTap: () {
                                      if (sessionIdController.text.isNotEmpty) {
                                        sessServiceRM.setState(
                                            (state) => state.getSession(
                                                sessionIdController.text),
                                            onData: (context, sessServiceRM) {
                                          if (sessServiceRM.session != null) {
                                            Navigator.pushNamed(
                                                context, 'quiz/begin',
                                                arguments:
                                                    sessServiceRM.session);
                                          }
                                        }, onError: (context, e) {
                                          ErrorHandler.showErrorDialog(
                                              context, e);
                                        });
                                      }
                                    },
                                    child: Icon(
                                      Icons.play_arrow,
                                      size: 60,
                                      color: bg_dark,
                                    ),
                                  )))
                        ]);
                  })))
      // SizedBox(width: 30),
    ]);
  }
}
