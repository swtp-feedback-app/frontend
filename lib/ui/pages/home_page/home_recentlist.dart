import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/pages/home_page/home_recentlist_item.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class HomeRecentList extends StatelessWidget {
  HomeRecentList({this.heading = "Aktuelle Kurse", this.sessions});

  final String heading;
  final List<Session> sessions;
  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(0, 25, 0, 25),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            'Aktuelle Sessions',
            textAlign: TextAlign.start,
            style: headline4,
          ),
          sessions.isNotEmpty
              ? Container(
                  //constraints: BoxConstraints(maxHeight: 400),
                  padding: EdgeInsets.only(bottom: 10),
                  alignment: Alignment.topLeft,
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: sessions.length,
                    itemBuilder: (context, index) => RecentListItem(
                      session: sessions[index],
                      onTap: () {
                        Navigator.pushNamed(context, 'quiz/begin',
                            arguments: sessions[index]);
                      },
                    ),
                  ))
              : Padding(
                  padding: const EdgeInsets.only(top: 8.0),
                  child: Text(
                    "Zur Zeit sind keine Sessions zu bearbeiten",
                    style: headline5,
                    textAlign: TextAlign.center,
                  ),
                ),
        ],
      ),
    );
  }
}
