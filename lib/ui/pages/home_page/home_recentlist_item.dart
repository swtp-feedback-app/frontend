import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

class RecentListItem extends StatelessWidget {
  final Session session;
  final Function onTap;

  const RecentListItem({this.session, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          heading: session.quiz.name,
          subHeading: 'Offen bis: ${session.getLocaleDatetime(session.end)}',
          icon: Icon(Icons.fast_forward, color: THMGrey),
        ));
  }
}
