import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class QuizHeader extends StatelessWidget {
  QuizHeader({this.currentQuestion, this.countQuestion, this.quizName});

  final user = Injector.get<AuthenticationService>().user;

  final int currentQuestion;
  final int countQuestion;
  final String quizName;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      padding: EdgeInsets.only(bottom: 15),
      child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Text(
                '$currentQuestion/$countQuestion',
                style: TextStyle(color: greyLetters, fontSize: 20),
              ),
              Stack(children: <Widget>[
                Container(
                  height: 10,
                  constraints: ((MediaQuery.of(context).size.width) < 768)
                      ? BoxConstraints(
                          minWidth: 100, maxWidth: screenSize.width * 0.5)
                      : BoxConstraints(minWidth: 100, maxWidth: 300),
                  width: screenSize.width * 0.5,
                  decoration: BoxDecoration(
                    color: Colors.grey.withOpacity(0.4),
                    borderRadius: BorderRadius.circular(5),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.1),
                        spreadRadius: 3,
                        blurRadius: 5,
                        offset: Offset(0, 5),
                      ),
                    ],
                  ),
                ),
                countQuestion > 0
                    ? Container(
                        height: 10,
                        constraints: ((MediaQuery.of(context).size.width) < 768)
                            ? BoxConstraints(
                                minWidth: 0,
                                maxWidth: screenSize.width *
                                    0.5 *
                                    (currentQuestion / countQuestion))
                            : BoxConstraints(
                                minWidth: 0,
                                maxWidth:
                                    300 * (currentQuestion / countQuestion)),
                        decoration: BoxDecoration(
                          color: theme.highlightColor,
                          borderRadius: BorderRadius.circular(5),
                          boxShadow: [
                            BoxShadow(
                              color: Colors.grey.withOpacity(0.1),
                              spreadRadius: 3,
                              blurRadius: 5,
                              offset: Offset(0, 5),
                            ),
                          ],
                        ),
                      )
                    : Container(),
              ]),
              Container(
                child: InkResponse(
                  highlightColor: Colors.transparent,
                  onTap: () {
                    Injector.get<SessionService>().resetSession();
                    Navigator.pop(context);
                  },
                  child: CircleAvatar(
                    backgroundColor: theme.focusColor,
                    radius: 20,
                    child: Icon(
                      Icons.clear,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Center(
            child: Text(
              quizName,
              style: headline5,
            ),
          )
        ],
      ),
    );
  }
}
