import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WordcloudInput extends StatefulWidget {
  final WordCloudQuestion question;
  final void Function(Question q) onQuestionChanged;
  final Key key;

  WordcloudInput(this.question, this.onQuestionChanged, this.key);

  @override
  _WordcloudInputState createState() =>
      _WordcloudInputState(question, onQuestionChanged, key);
}

class _WordcloudInputState extends State<WordcloudInput> {
  WordCloudQuestion question;
  final Key key;

  final void Function(Question q) onQuestionChanged;

  _WordcloudInputState(this.question, this.onQuestionChanged, this.key);

  @override
  Widget build(BuildContext context) {
    return Column(
        key: key,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Container(
            child: Column(
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                  child: Text("Füge der Wordcloud einen Begriff hinzu."),
                ),
                Container(
                  padding: EdgeInsets.fromLTRB(25, 10, 25, 10),
                  child: Text(question.question,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontSize: 20,
                        fontWeight: FontWeight.bold,
                        color: font_dark,
                      )),
                )
              ],
            ),
          ),
          UIHelper.verticalSpaceLarge(),
          Container(
            padding: EdgeInsets.fromLTRB(0, 25, 0, 25),
            constraints: BoxConstraints(
                minHeight: 100,
                maxHeight: MediaQuery.of(context).size.height / 3),
            child: Container(
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(12),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.grey.withOpacity(0.2),
                        spreadRadius: 3,
                        blurRadius: 5,
                        offset: Offset(0, 5),
                      ),
                    ]),
                child: TextField(
                  textAlignVertical: TextAlignVertical.top,
                 // expands: true,
                  textInputAction: TextInputAction.done,
                  maxLines: 2,
                  maxLength: 64,
                  decoration: InputDecoration(
                    counterText: "",
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(5.0),
                        borderSide: BorderSide(
                          color: green,
                          style: BorderStyle.solid,
                        )),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(5.0),
                      borderSide: BorderSide(
                        color: green,
                        style: BorderStyle.solid,
                      ),
                    ),
                  ),
                  onChanged: (text) {
                    question.answers[0].answer = text.replaceAll("\n", "");
                    onQuestionChanged(question);
                  },
                )),
          ),
          SizedBox(
            height: 30,
          ),
        ]);
  }
}
