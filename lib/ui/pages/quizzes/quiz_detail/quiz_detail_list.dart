import 'package:fbs_app/domain/entities/quiz.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_detail/quiz_detail_sessions_list_item.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class QuizDetailSessionsList extends StatelessWidget {
  QuizDetailSessionsList({this.sessions, this.quiz});

  final List<Session> sessions;
  final Quiz quiz;

  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    return sessions.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.zero,
              shrinkWrap: true,
              itemCount: sessions.length,
              itemBuilder: (context, index) => QuizDetailSessionsListItem(
                session: sessions[index],
                onTap: () {
                  IN.get<SessionService>().comesFromCourse = true;
                  Navigator.pushNamed(context, 'me/sessions/detail',
                      arguments: sessions[index]);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Keine Sessions gefunden",
                style: headline3,
                textAlign: TextAlign.center,
              ),
              UIHelper.verticalSpaceMedium(),
              FbsButton(
                  onPressed: () => Navigator.pushNamed(
                      context, 'quiz/newsession',
                      arguments: this.quiz),
                  text: "Session erstellen",
                  type: "primary",
                  size: "big")
            ],
          )));
  }
}
