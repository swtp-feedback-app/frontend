import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/domain/entities/question/question.dart';
import 'package:fbs_app/domain/entities/question/ratingQuestion.dart';
import 'package:fbs_app/domain/exceptions/index.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_multiple_choice/quiz_multiple_choice_page.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_order/quiz_order_page.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_wordcloud/quiz_wordcloud_page.dart';
import 'package:fbs_app/ui/pages/quizzes/survey/question_freetext.dart';
import 'package:fbs_app/ui/pages/quizzes/survey/question_rating.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:fbs_app/ui/pages/quizzes/quiz_common/quiz_header.dart';

import 'package:states_rebuilder/states_rebuilder.dart';

class QuizActivePage extends StatefulWidget {
  final Key wordcloudKey = new UniqueKey();
  final Session session;

  QuizActivePage(this.session);

  _QuizActive createState() => _QuizActive(wordcloudKey, session);
}

class _QuizActive extends State<QuizActivePage> {
  int index = 0;
  final session;
  Key wckey;

  _QuizActive(this.wckey, this.session);

  void initState() {
    index = session.beginAtQuestion;
    super.initState();
  }

  //callback-function: if answer has changed, update questions to reflect that
  void onQuestionChange(Question q) {
    session.quiz.questions[index] = q;
  }

  Future<void> submitAnswer([TextEditingController controller]) async {
    Question quest = session.quiz.questions[index];
    if (quest is MultipleChoiceQuestion) {
      List<MultipleChoiceAnswer> chosens = new List<MultipleChoiceAnswer>();
      for (MultipleChoiceAnswer a in quest.answers) {
        if (a.chosen) chosens.add(a);
      }
      await Injector.get<SessionService>()
          .submitAnswer(this.session, index + 1, chosens);
    } else if (quest is OpenQuestion) {
      await Injector.get<SessionService>()
          .submitAnswer(this.session, index + 1, quest.answers);
      wckey = new UniqueKey();
    } else if (quest is PairsQuestion) {
      // unimplemented in frontend
    } else if (quest is SortQuestion) {
      await Injector.get<SessionService>()
          .submitAnswer(this.session, index + 1, quest.answers);
    } else if (quest is WordCloudQuestion) {
      await Injector.get<SessionService>()
          .submitAnswer(this.session, index + 1, quest.answers);
      wckey = new UniqueKey();
    } else if (quest is RatingQuestion) {
      await Injector.get<SessionService>()
          .submitAnswer(this.session, index + 1, quest.answers);
    }
  }

  Widget buildQuestion() {
    Question quest = session.quiz.questions[index];
    if (quest is MultipleChoiceQuestion) {
      return QuizMultipleChoicePage(quest, onQuestionChange, UniqueKey());
    } else if (quest is OpenQuestion) {
      return OpenQuestionPage(quest, onQuestionChange, wckey);
    } else if (quest is PairsQuestion) {
      return Container();
    } else if (quest is SortQuestion) {
      return QuizOrderPage(quest, onQuestionChange, UniqueKey());
    } else if (quest is WordCloudQuestion) {
      return WordcloudInput(quest, onQuestionChange, wckey);
    } else if (quest is RatingQuestion) {
      return RatingQuestionPage(quest, onQuestionChange, UniqueKey());
    } else {
      return Container();
    }
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final _scrollController = ScrollController();
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        body: SafeArea(
          child: index >= session.quiz.questionCount
              ? Container()
              : Center(
                  child: Container(
                  padding: const EdgeInsets.fromLTRB(24, 24, 24, 0),
                  constraints: ((MediaQuery.of(context).size.width) > 768)
                      ? BoxConstraints(minWidth: 100, maxWidth: 500)
                      : null,
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      QuizHeader(
                        currentQuestion: index + 1,
                        countQuestion: session.quiz.questionCount,
                        quizName: session.quiz.name,
                      ),
                      Expanded(
                          child: Scrollbar(
                              isAlwaysShown: screenSize.aspectRatio >= 0.6,
                              controller: _scrollController,
                              child: SingleChildScrollView(
                                  controller: _scrollController,
                                  child: buildQuestion()))),
                      UIHelper.verticalSpaceMedium(),
                      WhenRebuilderOr<UserSessionStatisticsService>(
                        observe: () => RM.get<UserSessionStatisticsService>(),
                        onWaiting: () => Center(
                          child: CircularProgressIndicator(
                            valueColor:
                                new AlwaysStoppedAnimation<Color>(green),
                          ),
                        ),
                        builder: (_, usServiceRM) {
                          return Center(
                            child: FbsButton(
                                onPressed: () async {
                                  try {
                                    if (index >=
                                        session.quiz.questionCount - 1) {
                                      //get statistics and end quiz
                                      await submitAnswer();
                                      usServiceRM.setState(
                                          (state) => state.getStats(session),
                                          onData: (context, usServiceRM) {
                                        Navigator.of(context)
                                            .pushReplacementNamed('quiz/end',
                                                arguments: session.quiz.survey);
                                      }, onError: (context, e) {
                                        ErrorHandler.showErrorDialog(
                                            context, e);
                                      });
                                    } else {
                                      //go to next question
                                      await submitAnswer();
                                      setState(() {
                                        index++;
                                      });
                                    }
                                  } on UnauthorizedException catch (error) {
                                    Navigator.pop(context);
                                    ErrorHandler.showErrorDialog(
                                        context, error);
                                  } catch (error) {
                                    ErrorHandler.showErrorDialog(
                                        context, error);
                                  }
                                },
                                text: 'Bestätigen',
                                type: 'primary',
                                size: 'big'),
                          );
                        },
                      ),
                      UIHelper.verticalSpaceMedium()
                    ],
                  ),
                )),
        ),
      ),
    );
  }
}
