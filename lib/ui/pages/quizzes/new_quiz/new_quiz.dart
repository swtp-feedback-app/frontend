import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/domain/entities/question/question.dart';
import 'package:fbs_app/domain/entities/question/ratingQuestion.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/create_quiz_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class NewQuestion extends StatefulWidget {
  NewQuestion(this.quiz);

  final TextEditingController questionController = new TextEditingController();
  final TextEditingController answer1Controller = new TextEditingController();
  final TextEditingController answer2Controller = new TextEditingController();
  final TextEditingController answer3Controller = new TextEditingController();
  final TextEditingController answer4Controller = new TextEditingController();
  final Key key = new UniqueKey();
  final Quiz quiz;

  _NewQuestionState createState() => _NewQuestionState(
      //if quiz came from newQuiz: get from createQuizService, else (when editing quiz): quiz has an id
      this.quiz.id == 0 ? Injector.get<CreateQuizService>().quiz : quiz,
      this.questionController,
      this.answer1Controller,
      this.answer2Controller,
      this.answer3Controller,
      this.answer4Controller,
      this.quiz.id == 0 ? false : true,
      this.key);
}

class _NewQuestionState extends State<NewQuestion> {
  _NewQuestionState(
      this.quiz,
      this.questionController,
      this.answer1Controller,
      this.answer2Controller,
      this.answer3Controller,
      this.answer4Controller,
      this.update,
      this.key) {
    //check if quiz has questions
    if (update && quiz.questions.length > 0) {
      question = quiz.questions[0];
    }
    //initial question type
    if (quiz.survey) {
      questionType = "Bewertungsskala";
    } else {
      questionType = "Multiple Choice";
    }
  }

  String questionType; //starting value for dropdown menu
  int number = 0; //starting value for number of question
  Question question; //current question on display
  Key key;
  GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  final TextEditingController questionController;
  final TextEditingController answer1Controller;
  final TextEditingController answer2Controller;
  final TextEditingController answer3Controller;
  final TextEditingController answer4Controller;


  final user = Injector.get<AuthenticationService>().user;
  final quiz; //the quiz to be edited or to be created

  bool _validate = true; //to check if all textfields have been filled out
  bool update = false; //to check if question should be added or just updated
  bool controllerSet = false; //otherwise texts get reset at every rebuild

  List<bool> corrects = [
    false,
    false,
    false,
    false
  ]; //indices for multiple choice question-checkboxes: which ones are correct


  //submit question: process text fields
  bool handleSubmitted() {
    bool replace = !(question == null);
    //if true, question already exists and has to be replaced with new values
    //check if all textfields have been filled out and if so, prepare question for submit
    setState(() {
      if (questionType == "Multiple Choice") {
        _validate = questionController.text.trim().isNotEmpty &&
            answer1Controller.text.trim().isNotEmpty &&
            answer2Controller.text.trim().isNotEmpty &&
            answer3Controller.text.trim().isNotEmpty &&
            answer4Controller.text.trim().isNotEmpty;
        List<MultipleChoiceAnswer> answers = [
          new MultipleChoiceAnswer(answer1Controller.text, corrects[0]),
          new MultipleChoiceAnswer(answer2Controller.text, corrects[1]),
          new MultipleChoiceAnswer(answer3Controller.text, corrects[2]),
          new MultipleChoiceAnswer(answer4Controller.text, corrects[3])
        ];
        question = new MultipleChoiceQuestion(
            number: number + 1,
            question: questionController.text,
            answers: answers);
        corrects.clear();
        for (int i = 0; i <= 3; i++) {
          //reset checkboxes
          corrects.add(false);
        }
      } else if (questionType == "Reihenfolge") {
        _validate = questionController.text.trim().isNotEmpty &&
            answer1Controller.text.trim().isNotEmpty &&
            answer2Controller.text.trim().isNotEmpty &&
            answer3Controller.text.trim().isNotEmpty &&
            answer4Controller.text.trim().isNotEmpty;
        List<SortAnswer> answers = [
          new SortAnswer(answer1Controller.text),
          new SortAnswer(answer2Controller.text),
          new SortAnswer(answer3Controller.text),
          new SortAnswer(answer4Controller.text),
        ];
        question = new SortQuestion(
            number: number + 1,
            question: questionController.text,
            answers: answers);
      } else if (questionType == "Wordcloud") {
        _validate = questionController.text.trim().isNotEmpty;
        question = new WordCloudQuestion(
            number: number + 1, question: questionController.text);
      } else if (questionType == "Freitext") {
        _validate = questionController.text.trim().isNotEmpty;
        question = new OpenQuestion(
            number: number + 1, question: questionController.text);
      } else if (questionType == "Bewertungsskala") {
        _validate = questionController.text.trim().isNotEmpty;
        question = new RatingQuestion(
            number: number + 1, question: questionController.text);
      }
      //if all textfields have been filled out: prepare question for submit
      if (_validate) {
        if (replace == false) {
          quiz.questions.add(question);
        } else {
          quiz.questions.replaceRange(number, number + 1, [question]);
        }
        quiz.questionCount++;
        clearTextFields();
      }
    });
    return _validate;
  }

  //save changes so they don't get lost on a rebuild when keyboard is closed
  void changedAnswer(TextEditingController controller, String text) {
    if (question is MultipleChoiceQuestion) {
      MultipleChoiceQuestion quest = question;
      if (controller == answer1Controller) {
        MultipleChoiceAnswer ans =
            new MultipleChoiceAnswer(answer1Controller.text, corrects[0]);
        quest.answers.replaceRange(0, 1, [ans]);
      } else if (controller == answer2Controller) {
        MultipleChoiceAnswer ans =
            new MultipleChoiceAnswer(answer2Controller.text, corrects[1]);
        quest.answers.replaceRange(1, 2, [ans]);
      } else if (controller == answer3Controller) {
        MultipleChoiceAnswer ans =
            new MultipleChoiceAnswer(answer3Controller.text, corrects[2]);
        quest.answers.replaceRange(2, 3, [ans]);
      } else if (controller == answer4Controller) {
        MultipleChoiceAnswer ans =
            new MultipleChoiceAnswer(answer4Controller.text, corrects[3]);
        quest.answers.replaceRange(3, 4, [ans]);
      }
      question = quest;
    } else if (question is SortQuestion) {
      SortQuestion quest = question;
      if (controller == answer1Controller) {
        SortAnswer ans = new SortAnswer(answer1Controller.text);
        quest.answers.replaceRange(0, 1, [ans]);
      } else if (controller == answer2Controller) {
        SortAnswer ans = new SortAnswer(answer2Controller.text);
        quest.answers.replaceRange(1, 2, [ans]);
      } else if (controller == answer3Controller) {
        SortAnswer ans = new SortAnswer(answer3Controller.text);
        quest.answers.replaceRange(2, 3, [ans]);
      } else if (controller == answer4Controller) {
        SortAnswer ans = new SortAnswer(answer4Controller.text);
        quest.answers.replaceRange(3, 4, [ans]);
      }
    }
  }

  //reset text fields after each submit
  void clearTextFields() {
    questionController.clear();
    answer1Controller.clear();
    answer2Controller.clear();
    answer3Controller.clear();
    answer4Controller.clear();
    if (quiz.survey) {
      questionType = "Bewertungsskala";
    } else {
      questionType = "Multiple Choice";
    }
  }

  //widget for multiple choice: text input and checkbox to mark correct answers
  Widget _answerCheck(
      BuildContext context, TextEditingController answer, int index) {
    return Row(
      children: [
        Expanded(
          child: _input(context, answer, "Antwort eingeben"),
        ),
        new Checkbox(
            value: corrects[index],
            activeColor: Color(0xFF80BA23),
            onChanged: (bool newValue) {
              setState(() {
                corrects[index] = newValue;
                if (update) {
                    var quest = question
                        as MultipleChoiceQuestion;
                  quest = question;
                  quest.answers[index].correct = newValue;
                  question = quest;
                  quiz.questions.replaceRange(number, number + 1, [question]);
                }
              });
            }),
      ],
    );
  }

  //widget for answers without checkboxes
  Widget _answerPlain(BuildContext context, TextEditingController answer) {
    return _input(context, answer, "Antwort eingeben");
  }

  //text field for question and answers
  Widget _input(
      BuildContext context, TextEditingController controller, String hinttext) {
    return Container(
        padding: EdgeInsets.fromLTRB(0, 10, 25, 15),
        child: TextFormField(
          controller: controller,
          maxLength: controller == questionController ? 100 : 64,
          onChanged: (text) {
            if (update) {
              changedAnswer(controller, text);
            }
          },
          decoration: InputDecoration(
            hintText: hinttext,
            counterText: '',
            fillColor: Colors.white,
            focusedBorder: OutlineInputBorder(
                borderSide:
                    const BorderSide(color: Color(0xFF80BA23), width: 2.0)),
          ),
        ));
  }

  //returns build depending on question type
  Widget getScheme(BuildContext context) {
    if (questionType == "Multiple Choice") {
      return Column(
        key: key,
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Frage",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF394A59),
              )),
          Container(
            constraints: BoxConstraints(minWidth: 150, maxWidth: 500),
            padding: EdgeInsets.all(10),
            child: _input(context, questionController, "Frage eingeben"),
          ),
          Padding(
            padding: EdgeInsets.all(15),
          ),
          Text("Antworten",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF394A59),
              )),
          Text("Markiere die korrekten Antworten mit der jeweiligen Checkbox."),
          _answerCheck(context, answer1Controller, 0),
          _answerCheck(context, answer2Controller, 1),
          _answerCheck(context, answer3Controller, 2),
          _answerCheck(context, answer4Controller, 3),
          //_buildAnswersWeb(context),
          Padding(
            padding: EdgeInsets.all(15),
          ),
        ],
      );
    } else if (questionType == "Reihenfolge") {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Frage",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF394A59),
              )),
          Container(
            constraints: BoxConstraints(minWidth: 150, maxWidth: 500),
            padding: EdgeInsets.all(10),
            child: _input(context, questionController, "Frage eingeben"),
          ),
          Padding(
            padding: EdgeInsets.all(15),
          ),
          Text("Antworten",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF394A59),
              )),
          Text("Gebe die Antworten in der korrekten Reihenfolge an."),
          _answerPlain(context, answer1Controller),
          _answerPlain(context, answer2Controller),
          _answerPlain(context, answer3Controller),
          _answerPlain(context, answer4Controller),
          Padding(
            padding: EdgeInsets.all(15),
          ),
        ],
      );
    } else if (questionType == "Wordcloud") {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Frage",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF394A59),
              )),
          Container(
            constraints: BoxConstraints(minWidth: 150, maxWidth: 500),
            padding: EdgeInsets.all(10),
            child: _input(
                context, questionController, "Aufgabenstellung eingeben"),
          ),
          Padding(
            padding: EdgeInsets.all(15),
          ),
        ],
      );
    } else if (questionType == "Freitext") {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Frage",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF394A59),
              )),
          Container(
            constraints: BoxConstraints(minWidth: 150, maxWidth: 500),
            padding: EdgeInsets.all(10),
            child: TextField(
              controller: questionController,
              textAlignVertical: TextAlignVertical.top,
              maxLength: 100,
              maxLines: 5,
              decoration: InputDecoration(
                hintText: "Aufgabenstellung eingeben",
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(
                      color: green,
                      style: BorderStyle.solid,
                    )),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(
                    color: green,
                    style: BorderStyle.solid,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
          ),
        ],
      );
    } else if (questionType == "Bewertungsskala") {
      return Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text("Frage",
              style: TextStyle(
                fontSize: 20,
                fontWeight: FontWeight.bold,
                color: Color(0xFF394A59),
              )),
          Container(
            constraints: BoxConstraints(minWidth: 150, maxWidth: 500),
            padding: EdgeInsets.all(10),
            child: TextField(
              controller: questionController,
              textAlignVertical: TextAlignVertical.top,
              maxLength: 100,
              maxLines: 5,
              decoration: InputDecoration(
                hintText: "Aufgabenstellung eingeben",
                focusedBorder: OutlineInputBorder(
                    borderRadius: BorderRadius.circular(5.0),
                    borderSide: BorderSide(
                      color: green,
                      style: BorderStyle.solid,
                    )),
                border: OutlineInputBorder(
                  borderRadius: BorderRadius.circular(5.0),
                  borderSide: BorderSide(
                    color: green,
                    style: BorderStyle.solid,
                  ),
                ),
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.all(15),
          ),
          Text(
              'Zur Beantwortung steht eine Skala von "Trifft voll zu" bis "Trifft gar nicht zu" zur Verfügung.'),
          Padding(
            padding: EdgeInsets.all(15),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  //list of all questions in this quiz
  Widget getOverview(BuildContext context) {
    return WhenRebuilderOr<CreateQuizService>(
      observe: () => RM.get<CreateQuizService>(),
      onWaiting: () => Center(
        child: CircularProgressIndicator(
          valueColor: new AlwaysStoppedAnimation<Color>(green),
        ),
      ),
      builder: (_, createQuizServiceRM) {
        return ReorderableListView(
            onReorder: ((int oldIndex, int newIndex) {
              setState(
                () {
                  Question oldQ = quiz.questions[oldIndex];
                  Question newQ = quiz.questions[newIndex];
                  if (newIndex > oldIndex) {
                    newIndex -= 1;
                  }
                  int oldN = oldQ.number;
                  oldQ.number = newQ.number;
                  newQ.number = oldN;
                  try {
                    createQuizServiceRM.setState(
                      (state) => state.submitQuestion(
                          quiz, oldQ, user.authToken, true),
                      onData: (context, createQuizServiceRM) {},
                    );
                    createQuizServiceRM.setState(
                      (state) => state.submitQuestion(
                          quiz, newQ, user.authToken, true),
                      onData: (context, createQuizServiceRM) {},
                    );
                  } catch (e) {
                    ErrorHandler.showErrorDialog(context, e);
                  }
                  final Question item = quiz.questions.removeAt(oldIndex);
                  quiz.questions.insert(newIndex, item);
                },
              );
            }),
            header: SafeArea(
              child: Card(
                color: green,
                key: ValueKey(-1),
                child: ListTile(
                  contentPadding: EdgeInsets.all(10),
                  title: Text(
                    "Übersicht",
                    style: TextStyle(
                      fontSize: 20,
                      color: Colors.white,
                    ),
                  ),
                ),
              ),
            ),
            padding: EdgeInsets.all(5),
            children: <Widget>[
              for (Question item in quiz.questions)
                Card(
                  key: ValueKey(item),
                  child: ListTile(
                    contentPadding: EdgeInsets.all(10),
                    title: Text(
                      "Frage " + (quiz.questions.indexOf(item) + 1).toString(),
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontWeight: FontWeight.bold, color: font_dark),
                    ),
                    subtitle: Text(
                      item.question,
                    ),
                    onTap: () => {
                      setState(() {
                        controllerSet = false;
                        number = quiz.questions.indexOf(item);
                        question = quiz.questions[quiz.questions.indexOf(item)];
                        if (question is MultipleChoiceQuestion) {
                          questionType = "Multiple Choice";
                        } else if (question is SortQuestion) {
                          questionType = "Reihenfolge";
                        } else if (question is WordCloudQuestion) {
                          questionType = "Wordcloud";
                        } else if (question is RatingQuestion) {
                          questionType = "Bewertungsskala";
                        } else if (question is OpenQuestion) {
                          questionType = "Freitext";
                        }
                        (MediaQuery.of(context).size.width) > 768
                            ? null
                            : Navigator.pop(context);
                      })
                    },
                  ),
                )
            ]);
      },
    );
  }

  //drawer with list of all questions in this quiz for mobile
  Widget _questionsDrawer(BuildContext context) {
    return Drawer(
      child: getOverview(context),
    );
  }

  //Baut das Hauptwidget
  Widget _main(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          key: _drawerKey,
          appBar: FBSAppBar(
            title: "Quiz bearbeiten",
            action: InkWell(
              highlightColor: Colors.transparent,
              onTap: () {
                Navigator.pop(context);
                Navigator.pushReplacementNamed(context, 'me/quizzes');
              },
              child: CircleAvatar(
                backgroundColor: bg_dark,
                radius: 20,
                child: Icon(Icons.clear, color: Colors.white),
              ),
            ),
            leading: (MediaQuery.of(context).size.width) > 768
                ? Container()
                : InkWell(
                    highlightColor: Colors.transparent,
                    onTap: () {
                      FocusScope.of(context).unfocus();
                      _drawerKey.currentState.openDrawer();
                    },
                    child: CircleAvatar(
                      backgroundColor: bg_dark,
                      radius: 20,
                      child: Icon(Icons.menu, color: Colors.white),
                    ),
                  ),
          ),
          drawer: (MediaQuery.of(context).size.width) > 768
              ? null
              : _questionsDrawer(context),
          body: SafeArea(
              child: Container(
            //background
            color: Color(0xFFF2F4F5),
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: SingleChildScrollView(
              controller: new ScrollController(),
              child: Stack(
                children: <Widget>[
                  Container(
                    width: MediaQuery.of(context).size.width,
                    padding: EdgeInsets.fromLTRB(25, 0, 25, 0),
                    child: Flex(
                      direction: (MediaQuery.of(context).size.width) > 768
                          ? Axis.horizontal
                          : Axis.vertical,
                      children: <Widget>[
                        Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            (MediaQuery.of(context).size.width) > 768
                                ? Container(
                                    //alle Fragen - alternativ: in Drawer
                                    constraints: BoxConstraints(
                                        minWidth: 150, maxWidth: 300),
                                    height: MediaQuery.of(context).size.height -
                                        100,
                                    padding: EdgeInsets.all(0),
                                    child: getOverview(context),
                                  )
                                : Container(),
                          ],
                        ),
                        Container(
                            width: (MediaQuery.of(context).size.width) > 768
                                ? MediaQuery.of(context).size.width - 350
                                : null,
                            //300 for drawer, 50 for padding left and right
                            child: Center(
                              child: Container(
                                padding: EdgeInsets.all(25),
                                decoration: BoxDecoration(
                                  borderRadius:
                                      BorderRadius.all(Radius.circular(20)),
                                  color: Colors.white,
                                ),
                                constraints: BoxConstraints(
                                    minWidth: 150, maxWidth: 500),
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: <Widget>[
                                    Wrap(
                                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                                      spacing: 40,
                                      runSpacing: 5,
                                      children: [
                                        Row(
                                          mainAxisAlignment:
                                              MainAxisAlignment.start,
                                          mainAxisSize: MainAxisSize.min,
                                          children: [
                                            !update
                                                ? Text(
                                                    "Frage " +
                                                        (number + 1)
                                                            .toString() +
                                                        " von " +
                                                        (quiz.questions.length +
                                                                1)
                                                            .toString(),
                                                    style: TextStyle(
                                                        color: font_dark,
                                                        fontSize: ((MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width) >
                                                                768)
                                                            ? 16
                                                            : 14),
                                                  )
                                                : Text(
                                                    "Frage " +
                                                        (number + 1)
                                                            .toString() +
                                                        " von " +
                                                        (quiz.questions.length)
                                                            .toString(),
                                                    style: TextStyle(
                                                        color: font_dark,
                                                        fontSize: ((MediaQuery.of(
                                                                        context)
                                                                    .size
                                                                    .width) >
                                                                768)
                                                            ? 16
                                                            : 14),
                                                  ),
                                            WhenRebuilderOr<CreateQuizService>(
                                              observe: () =>
                                                  RM.get<CreateQuizService>(),
                                              onWaiting: () => Center(
                                                child:
                                                    CircularProgressIndicator(
                                                  valueColor:
                                                      new AlwaysStoppedAnimation<
                                                          Color>(green),
                                                ),
                                              ),
                                              builder:
                                                  (_, createQuizServiceRM) {
                                                return Container(
                                                  margin: EdgeInsets.all(0),
                                                  child: IconButton(
                                                    icon: Icon(Icons.delete),
                                                    color: red,
                                                    tooltip: 'Frage löschen',
                                                    onPressed: () {
                                                      setState(() {
                                                        if (!update) {
                                                          //question has not yet been saved
                                                          clearTextFields();
                                                        } else {
                                                          quiz.questions
                                                              .remove(question);
                                                          createQuizServiceRM
                                                              .setState(
                                                            (state) => state
                                                                .deleteQuestion(
                                                                    question,
                                                                    user.authToken),
                                                            onError:
                                                                (context, e) {
                                                              ErrorHandler
                                                                  .showErrorDialog(
                                                                      context,
                                                                      e);
                                                            },
                                                            onData: (context,
                                                                createQuizServiceRM) {},
                                                          );
                                                          clearTextFields();
                                                        }
                                                      });
                                                    },
                                                  ),
                                                );
                                              },
                                            ),
                                          ],
                                        ),
                                        Container(
                                          constraints:
                                              BoxConstraints(maxWidth: 125),
                                          child: WhenRebuilderOr<
                                              CreateQuizService>(
                                            observe: () =>
                                                RM.get<CreateQuizService>(),
                                            onWaiting: () => Center(
                                              child: CircularProgressIndicator(
                                                valueColor:
                                                    new AlwaysStoppedAnimation<
                                                        Color>(green),
                                              ),
                                            ),
                                            builder: (_, createQuizServiceRM) {
                                              return Container(
                                                child: FbsButton(
                                                    size: "small",
                                                    type: "primary",
                                                    text: "Beenden",
                                                    onPressed: () {
                                                      Navigator.pop(context);
                                                      Navigator
                                                          .pushReplacementNamed(
                                                              context,
                                                              'me/quizzes');
                                                    }),
                                              );
                                            },
                                          ),
                                        )
                                      ],
                                    ),
                                    Row(
                                      children: [
                                        Text("Fragetyp: ",
                                            style: TextStyle(
                                                fontWeight: FontWeight.bold,
                                                color: font_dark)),
                                        new DropdownButton<String>(
                                          style: TextStyle(
                                              color: font_dark,
                                              fontSize: ((MediaQuery.of(context)
                                                          .size
                                                          .width) >
                                                      768)
                                                  ? 16
                                                  : 14),
                                          focusColor: Colors.transparent,
                                          items: !quiz.survey
                                              ? <String>[
                                                  'Multiple Choice',
                                                  'Reihenfolge',
                                                  'Wordcloud',
                                                ].map((String value) {
                                                  return new DropdownMenuItem<
                                                      String>(
                                                    value: value,
                                                    child: new Text(
                                                      value,
                                                      style: TextStyle(
                                                          color: font_dark,
                                                          fontSize: ((MediaQuery.of(
                                                                          context)
                                                                      .size
                                                                      .width) >
                                                                  768)
                                                              ? 16
                                                              : 14),
                                                    ),
                                                  );
                                                }).toList()
                                              : <String>[
                                                  'Bewertungsskala',
                                                  'Wordcloud',
                                                  'Freitext',
                                                ].map((String value) {
                                                  return new DropdownMenuItem<
                                                      String>(
                                                    value: value,
                                                    child: new Text(value,
                                                        style: TextStyle(
                                                            color: font_dark,
                                                            fontSize: ((MediaQuery.of(
                                                                            context)
                                                                        .size
                                                                        .width) >
                                                                    768)
                                                                ? 16
                                                                : 14)),
                                                  );
                                                }).toList(),
                                          onChanged: (String newValue) {
                                            setState(() {
                                              questionType = newValue;
                                            });
                                          },
                                          value: questionType,
                                        ),
                                      ],
                                    ),
                                    getScheme(context),
                                    !_validate
                                        ? Container(
                                            padding: EdgeInsets.fromLTRB(
                                                0, 0, 0, 25),
                                            child: Text(
                                                "Alle Felder sind auszufüllen.",
                                                style: TextStyle(
                                                    color: red, fontSize: 16)),
                                          )
                                        : Container(),
                                    Row(
                                      mainAxisSize: MainAxisSize.min,
                                      children: [
                                        WhenRebuilderOr<CreateQuizService>(
                                          observe: () =>
                                              RM.get<CreateQuizService>(),
                                          onWaiting: () => Center(
                                            child: CircularProgressIndicator(
                                              valueColor:
                                                  new AlwaysStoppedAnimation<
                                                      Color>(green),
                                            ),
                                          ),
                                          builder: (_, createQuizServiceRM) {
                                            return Expanded(
                                                child: FbsButton(
                                              text: "Frage bestätigen",
                                              type: "primary",
                                              size: "big",
                                              onPressed: () {
                                                FocusScope.of(context)
                                                    .unfocus();
                                                if (handleSubmitted()) {
                                                  createQuizServiceRM.setState(
                                                    (state) =>
                                                        state.submitQuestion(
                                                            quiz,
                                                            question,
                                                            user.authToken,
                                                            update),
                                                    onError: (context, error) =>
                                                        ErrorHandler
                                                            .showErrorDialog(
                                                                context, error),
                                                    onData: (context,
                                                        createQuizServiceRM) {},
                                                  );
                                                }
                                                if (_validate) {}
                                                controllerSet = false;
                                                if ((number + 1) <
                                                    quiz.questions.length) {
                                                  //next question is already there
                                                  question = quiz
                                                      .questions[number + 1];
                                                  update = true;
                                                  number++;
                                                } else {
                                                  number =
                                                      quiz.questions.length;
                                                }
                                              },
                                            ));
                                          },
                                        ),
                                      ],
                                    ),
                                    Container(
                                      constraints: BoxConstraints(
                                          minWidth: 150, maxWidth: 500),
                                    )
                                  ],
                                ),
                              ),
                            ))
                      ],
                    ),
                  )
                ],
              ),
            ),
          ))),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (number >= quiz.questions.length) {
      question = null;
      update = false;
    }
    if ((question != null && !controllerSet) || update) {
      setControllers();
    }
    return _main(context);
  }

  //if question already exists and just has to be edited: fill text fields etc. with the appropriate values
  void setControllers() {
    update = true;
    controllerSet = true;
    questionController.text = question.question;

    if (question is MultipleChoiceQuestion) {
      MultipleChoiceQuestion q = question;
      questionType = "Multiple Choice";
      answer1Controller.text = q.answers[0].answer;
      answer2Controller.text = q.answers[1].answer;
      answer3Controller.text = q.answers[2].answer;
      answer4Controller.text = q.answers[3].answer;
      corrects = [
        q.answers[0].correct,
        q.answers[1].correct,
        q.answers[2].correct,
        q.answers[3].correct
      ];
    } else if (question is SortQuestion) {
      SortQuestion q = question;
      questionType = "Reihenfolge";
      answer1Controller.text = q.answers[0].answer;
      answer2Controller.text = q.answers[1].answer;
      answer3Controller.text = q.answers[2].answer;
      answer4Controller.text = q.answers[3].answer;
    } else if (question is WordCloudQuestion) {
      questionType = "Wordcloud";
    } else if (question is OpenQuestion) {
      questionType = "Freitext";
    } else if (question is RatingQuestion) {
      questionType = "Bewertungsskala";
    }
  }
}
