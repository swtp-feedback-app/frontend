import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/domain/entities/quiz.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/service/create_quiz_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'dart:async';
import 'package:intl/intl.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class NewQuizStart extends StatefulWidget {
  NewQuizStart(this.course);

  final Course course;

  @override
  NewQuizPage createState() => NewQuizPage(
      this.course != null ? course : new Course(name: "keinen Kurs auswählen"),
      this.course != null ? true : false);
}

class NewQuizPage extends State<NewQuizStart> {
  TextEditingController quiztitleController = new TextEditingController();
  TextEditingController descriptionController = new TextEditingController();
  TextEditingController descSessionController = new TextEditingController();

  NewQuizPage(this.selectedCourse, this.courseGiven);

  final user = Injector.get<AuthenticationService>().user;
  final bool courseGiven; //was course given? -> quiz can only belong to that specific course

  bool addSession = false; //bool for checkbox to add session
  List<Course> courses = new List<Course>(); //all courses quiz can be assigned to
  Course selectedCourse; //selected course
  int quizType; //quiz or survey
  int anon; //participation anonymous or logged in
  bool _validate = false; //check if all essential text fields have been filled in
  bool _validateSession = false; //check if all essential text fields to create a session have been filled in

  //to choose in calendar
  DateTime selectedStartDate;
  TimeOfDay selectedStartTime;
  DateTime selectedEndDate;
  TimeOfDay selectedEndTime;

  //to get duration
  DateTime start; //combination of selectedStartDate and selectedStartTime
  DateTime end; //combination of selectedEndDate and selectedEndTime
  Duration duration; //diff between start and end

  //return duration as a formatted string to display in the ui
  String _showDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitHours = twoDigits(duration.inHours.remainder(24));
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    return duration.inDays.toString() +
        " Tage, $twoDigitHours Stunden, $twoDigitMinutes Minuten";
  }

  //select start date
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime(2020, 1),
      lastDate: DateTime(2040, 12),
      initialDate: (selectedStartDate == null) ? new DateTime.now() : selectedStartDate,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedStartDate)
      setState(() {
        selectedStartDate = picked;
        if (selectedStartDate != null && selectedStartTime != null) {
          start = new DateTime(selectedStartDate.year, selectedStartDate.month,
              selectedStartDate.day, selectedStartTime.hour, selectedStartTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  //select end date
  Future<Null> _selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime(2020, 1),
      lastDate: DateTime(2040, 12),
      initialDate:
          (selectedEndDate == null) ? new DateTime.now() : selectedEndDate,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null)
      setState(() {
        selectedEndDate = picked;
        if (selectedEndDate != null && selectedEndTime != null) {
          end = new DateTime(
              selectedEndDate.year,
              selectedEndDate.month,
              selectedEndDate.day,
              selectedEndTime.hour,
              selectedEndTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  //select start time
  Future<void> _selectTime(BuildContext context) async {
    //0: start; 1: end
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: (selectedStartTime == null)
          ? TimeOfDay.fromDateTime(DateTime.now())
          : selectedStartTime,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null)
      setState(() {
        selectedStartTime = picked;
        if (selectedStartDate != null &&
            TimeOfDay.fromDateTime(DateTime.now()) != null) {
          start = new DateTime(selectedStartDate.year, selectedStartDate.month,
              selectedStartDate.day, selectedStartTime.hour, selectedStartTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  //select end time
  Future<void> _selectEndTime(BuildContext context) async {
    //0: start; 1: end
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime:
          (selectedEndTime == null) ? TimeOfDay.now() : selectedEndTime,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedEndTime)
      setState(() {
        selectedEndTime = picked;
        if (selectedEndDate != null && selectedEndTime != null) {
          end = new DateTime(
              selectedEndDate.year,
              selectedEndDate.month,
              selectedEndDate.day,
              selectedEndTime.hour,
              selectedEndTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  //check if all essential text fields have been filled in
  bool validateTextFields() {
    setState(() {
      _validate = quiztitleController.text.trim().isEmpty ||
          descriptionController.text.trim().isEmpty ||
          quizType == null;
    });
    return !_validate;
  }

  //check if all essential text fields to create a session have been filled in
  void validateSession() {
    if (courseGiven) {
      anon = 1;
    }
    setState(() {
      _validateSession = (anon == null ||
          selectedStartDate == null ||
          selectedStartTime == null ||
          selectedEndDate == null ||
          selectedEndTime == null ||
          duration.isNegative);
    });
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: FBSAppBar(title: "Quiz erstellen"),
        body: SingleChildScrollView(
            child: Stack(
          children: [
            Container(
              //background
              width: (MediaQuery.of(context).size.width),
              color: bg_light,
            ),
            SafeArea(
                child: Center(
              child: Container(
                  padding: EdgeInsets.all(25),
                  constraints: ((MediaQuery.of(context).size.width) > 768)
                      ? BoxConstraints(minWidth: 100, maxWidth: 500)
                      : null,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 0, 0, 0),
                        child: Text(
                          "Quizart auswählen",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 16,
                              color: font_dark,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Row(
                        children: [
                          new Radio(
                            value: 0,
                            activeColor: green,
                            groupValue: quizType,
                            onChanged: (int value) {
                              setState(() {
                                quizType = value;
                              });
                            },
                          ),
                          new Text(
                            'Quiz',
                            style: new TextStyle(fontSize: 16.0),
                          ),
                          new Radio(
                            value: 1,
                            activeColor: green,
                            groupValue: quizType,
                            onChanged: (int value) {
                              setState(() {
                                quizType = value;
                              });
                            },
                          ),
                          new Text(
                            'Umfrage',
                            style: new TextStyle(
                              fontSize: 16.0,
                            ),
                          ),
                        ],
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                        child: Text(
                          "Quiztitel",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 16,
                              color: font_dark,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                        child: Container(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: TextField(
                              controller: quiztitleController,
                              maxLength: 35,
                              decoration: InputDecoration(
                                counterText: '',
                                hintText: "Quiztitel eingeben",
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(
                                      color: green,
                                      style: BorderStyle.solid,
                                    )),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: green,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                              ),
                            )),
                      ),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                        child: Text(
                          "Beschreibung",
                          textAlign: TextAlign.start,
                          style: TextStyle(
                              fontSize: 16,
                              color: font_dark,
                              fontWeight: FontWeight.bold),
                        ),
                      ),
                      Container(
                          padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                          constraints:
                              BoxConstraints(minHeight: 50, maxHeight: 100),
                          child: TextField(
                            textInputAction: TextInputAction.done,
                            controller: descriptionController,
                            textAlignVertical: TextAlignVertical.top,
                            maxLines: 5,
                            decoration: InputDecoration(
                              hintText: "Beschreibung eingeben",
                              focusedBorder: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: green,
                                    style: BorderStyle.solid,
                                  )),
                              border: OutlineInputBorder(
                                borderRadius: BorderRadius.circular(5.0),
                                borderSide: BorderSide(
                                  color: green,
                                  style: BorderStyle.solid,
                                ),
                              ),
                            ),
                          )),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Container(
                            padding: EdgeInsets.fromLTRB(0, 15, 0, 10),
                            child: Text(
                              "Session erstellen",
                              textAlign: TextAlign.start,
                              style: TextStyle(
                                  fontSize: 20,
                                  color: font_dark,
                                  fontWeight: FontWeight.bold),
                            ),
                          ),
                          new Checkbox(
                              value: addSession,
                              activeColor: Color(0xFF80BA23),
                              onChanged: (bool newValue) {
                                setState(() {
                                  addSession = newValue;
                                });
                              }),
                        ],
                      ),
                      addSession
                          ? Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                    "Die folgenden Felder beziehen sich auf die Session, also wann und in welcher Weise die Teilnahme am Quiz möglich ist. Es ist optional, einen Kurs auszuwählen; eine Session für das Quiz kann jederzeit nachträglich eingetragen werden."),
                                Container(
                                  padding: EdgeInsets.fromLTRB(0, 20, 0, 0),
                                  child: Text(
                                    "Kurs zuordnen",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: font_dark,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                WhenRebuilderOr<CoursesService>(
                                  observe: () => RM.get<CoursesService>(),
                                  initState: (_, coursesServiceRM) {
                                    try {
                                      coursesServiceRM.setState(
                                        (state) => state.getAdminCoursesForUser(
                                            user.authToken),
                                        onData: (context, coursesServiceRM) {
                                          setState(() {
                                            this.courses =
                                                coursesServiceRM.courses;
                                            this.courses.add(selectedCourse);
                                          });
                                        },
                                      );
                                    } on Exception catch (e) {
                                      ErrorHandler.showErrorDialog(context, e);
                                    }
                                  },
                                  builder: (_, coursesService) {
                                    return Container();
                                  },
                                ),
                                !courseGiven
                                    ? new DropdownButton<Course>(
                                        isExpanded: true,
                                        style: TextStyle(color: font_dark),
                                        items: courses.map((Course course) {
                                          return new DropdownMenuItem<Course>(
                                            value: course,
                                            child: new Text(
                                              course.name,
                                              style: TextStyle(fontSize: 16),
                                            ),
                                          );
                                        }).toList(),
                                        onChanged: (Course newCourse) {
                                          setState(() {
                                            selectedCourse = newCourse;
                                            if (selectedCourse.id != null) {
                                              //no course selected
                                              anon = 1;
                                            } else {
                                              anon = 0;
                                            }
                                          });
                                        },
                                        value: selectedCourse,
                                      )
                                    : Text(
                                        selectedCourse.name,
                                        style: new TextStyle(
                                          fontSize: 16.0,
                                        ),
                                      ),
                                Container(
                                  padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                  child: Text(
                                    "Teilnahme",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: font_dark,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                selectedCourse.id == null
                                    ? Wrap(
                                        spacing:
                                            5.0, // gap between adjacent elements
                                        runSpacing: 5.0, //
                                        children: [
                                          Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              new Radio(
                                                value: 0,
                                                activeColor: green,
                                                groupValue: anon,
                                                onChanged: (int value) {
                                                  setState(() {
                                                    anon = value;
                                                  });
                                                },
                                              ),
                                              new Text(
                                                'anonym',
                                                style: new TextStyle(
                                                    fontSize: 16.0),
                                              ),
                                            ],
                                          ),
                                          Row(
                                            mainAxisSize: MainAxisSize.min,
                                            children: [
                                              new Radio(
                                                value: 1,
                                                activeColor: green,
                                                groupValue: anon,
                                                onChanged: (int value) {
                                                  setState(() {
                                                    anon = value;
                                                  });
                                                },
                                              ),
                                              new Text(
                                                'angemeldet',
                                                style: new TextStyle(
                                                  fontSize: 16.0,
                                                ),
                                              ),
                                            ],
                                          )
                                        ],
                                      )
                                    : Text('angemeldet',
                                        style: new TextStyle(fontSize: 16.0)),
                                Container(
                                  padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                                  child: Text(
                                    "Beschreibung",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: font_dark,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Container(
                                    padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                    constraints: BoxConstraints(
                                        minHeight: 50, maxHeight: 100),
                                    child: TextField(
                                      textInputAction: TextInputAction.done,
                                      controller: descSessionController,
                                      textAlignVertical: TextAlignVertical.top,
                                      maxLines: 5,
                                      decoration: InputDecoration(
                                        hintText:
                                            "Beschreibung eingeben (optional)",
                                        focusedBorder: OutlineInputBorder(
                                            borderRadius:
                                                BorderRadius.circular(5.0),
                                            borderSide: BorderSide(
                                              color: green,
                                              style: BorderStyle.solid,
                                            )),
                                        border: OutlineInputBorder(
                                          borderRadius:
                                              BorderRadius.circular(5.0),
                                          borderSide: BorderSide(
                                            color: green,
                                            style: BorderStyle.solid,
                                          ),
                                        ),
                                      ),
                                    )),
                                Container(
                                  padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                                  child: Text(
                                    "Freigabezeit und Dauer",
                                    textAlign: TextAlign.start,
                                    style: TextStyle(
                                        fontSize: 16,
                                        color: font_dark,
                                        fontWeight: FontWeight.bold),
                                  ),
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: InkWell(
                                        highlightColor: Colors.transparent,
                                        onTap: (() {
                                          _selectDate(context);
                                        }),
                                        child: new InputDecorator(
                                          decoration: new InputDecoration(
                                            labelText: "Start-Datum",
                                          ),
                                          child: (selectedStartDate != null)
                                              ? Text(DateFormat('yyyy-MM-dd')
                                                  .format(selectedStartDate))
                                              : Text(""),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width:
                                          (MediaQuery.of(context).size.width) /
                                              8,
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: InkWell(
                                        highlightColor: Colors.transparent,
                                        onTap: (() {
                                          _selectTime(context);
                                        }),
                                        child: new InputDecorator(
                                          decoration: new InputDecoration(
                                            labelText: "Start-Uhrzeit",
                                          ),
                                          child: new Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              (selectedStartTime != null)
                                                  ? new Text(selectedStartTime
                                                      .format(context))
                                                  : new Text(""),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Row(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    Expanded(
                                      flex: 4,
                                      child: InkWell(
                                        highlightColor: Colors.transparent,
                                        onTap: (() {
                                          _selectEndDate(context);
                                        }),
                                        child: new InputDecorator(
                                          decoration: new InputDecoration(
                                            labelText: "End-Datum",
                                          ),
                                          child: (selectedEndDate != null)
                                              ? Text(DateFormat('yyyy-MM-dd')
                                                  .format(selectedEndDate))
                                              : Text(""),
                                        ),
                                      ),
                                    ),
                                    SizedBox(
                                      width:
                                          (MediaQuery.of(context).size.width) /
                                              8,
                                    ),
                                    Expanded(
                                      flex: 4,
                                      child: InkWell(
                                        highlightColor: Colors.transparent,
                                        onTap: (() {
                                          _selectEndTime(context);
                                        }),
                                        child: new InputDecorator(
                                          decoration: new InputDecoration(
                                            labelText: "End-Uhrzeit",
                                          ),
                                          child: new Row(
                                            mainAxisAlignment:
                                                MainAxisAlignment.spaceBetween,
                                            mainAxisSize: MainAxisSize.min,
                                            children: <Widget>[
                                              (selectedEndTime != null)
                                                  ? new Text(selectedEndTime
                                                      .format(context))
                                                  : new Text(""),
                                            ],
                                          ),
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                                Container(
                                  padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                                  child: (duration != null)
                                      ? Text(
                                          "Dauer: " + _showDuration(duration))
                                      : Text("Dauer: -"),
                                ),
                              ],
                            )
                          : Container(),
                      (_validate || _validateSession)
                          ? Container(
                              padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                              child: Text(
                                "Quiztyp, Titel und Beschreibung sind auszufüllen. Sollte eine Session ausgewählt sein, muss Endzeit muss später als Anfangszeit sein.",
                                style: TextStyle(color: red, fontSize: 16),
                              ),
                            )
                          : Container(),
                      Container(
                        padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                        child: WhenRebuilderOr<CreateQuizService>(
                          observe: () => RM.get<CreateQuizService>(),
                          onWaiting: () => Center(
                            child: CircularProgressIndicator(
                              valueColor:
                                  new AlwaysStoppedAnimation<Color>(green),
                            ),
                          ),
                          dispose: (_, __) {
                            quiztitleController.dispose();
                            descriptionController.dispose();
                          },
                          builder: (_, createQuizServiceRM) {
                            return Center(
                              child: FbsButton(
                                text: "Starten",
                                type: "primary",
                                size: "big",
                                onPressed: () {
                                  Quiz q = new Quiz(
                                      id: 0,
                                      name: quiztitleController.text,
                                      description: descriptionController.text,
                                      questionCount: 0,
                                      survey: quizType == 0 ? false : true,
                                      instantFeedback: false);
                                  Session s;
                                  if (addSession) {
                                    validateSession();
                                    if (!_validateSession) {
                                      DateTime start = new DateTime(
                                          selectedStartDate.year,
                                          selectedStartDate.month,
                                          selectedStartDate.day,
                                          selectedStartTime.hour,
                                          selectedStartTime.minute);
                                      DateTime end = new DateTime(
                                          selectedEndDate.year,
                                          selectedEndDate.month,
                                          selectedEndDate.day,
                                          selectedEndTime.hour,
                                          selectedEndTime.minute);
                                      s = new Session(
                                          quiz: q,
                                          start: (start
                                                      .millisecondsSinceEpoch /
                                                  1000)
                                              .round(),
                                          end: (end
                                                      .millisecondsSinceEpoch /
                                                  1000)
                                              .round(),
                                          type: anon == 0
                                              ? "anonym"
                                              : "authenticated",
                                          description:
                                              descSessionController.text.isEmpty
                                                  ? ""
                                                  : descSessionController.text);
                                    }
                                  }
                                  if (validateTextFields() &&
                                      (!_validateSession || !addSession)) {
                                    createQuizServiceRM.setState(
                                      (state) => state.createQuiz(
                                          q,
                                          user.authToken,
                                          s != null ? s : null,
                                          selectedCourse.id != null
                                              ? selectedCourse
                                              : null),
                                      onError: (context, e) {
                                        ErrorHandler.showErrorDialog(
                                            context, e);
                                      },
                                      onData: (context, createQuizServiceRM) {
                                        Navigator.pushReplacementNamed(
                                            context, 'quiz/edit',
                                            arguments: q);
                                      },
                                    );
                                  }
                                },
                              ),
                            );
                          },
                        ),
                      )
                    ],
                  )),
            )),
          ],
        )),
      ),
    );
  }
}
