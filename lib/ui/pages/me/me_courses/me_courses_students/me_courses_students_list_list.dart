import 'package:fbs_app/domain/entities/user.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/pages/me/me_courses/me_courses_students/me_courses_students_list_item.dart';
import 'package:flutter/material.dart';

class MeCoursesStudentsList extends StatelessWidget {
  MeCoursesStudentsList({this.students});

  final List<User> students;

  @override
  Widget build(BuildContext context) {
    return students.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: students.length,
              itemBuilder: (context, index) => MeCoursesStudentsListItem(
                student: students[index],
                onTap: () {},
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "In diesem Kurs sind noch keine Studenten eingeschrieben",
                style: headline3,
                textAlign: TextAlign.center,
              ),
              UIHelper.verticalSpaceMedium(),
            ],
          )));
  }
}
