import 'package:fbs_app/domain/entities/user.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

class MeCoursesStudentsListItem extends StatelessWidget {
  final User student;
  final Function onTap;

  const MeCoursesStudentsListItem({this.student, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
            heading: student.firstname + ' ' + student.lastname,
            subHeading: student.username));
  }
}
