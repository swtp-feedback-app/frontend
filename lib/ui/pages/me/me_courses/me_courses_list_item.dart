import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

import 'package:fbs_app/domain/entities/course.dart';

import 'package:fbs_app/ui/common/app_theme.dart';

class MeCoursesListItem extends StatelessWidget {
  final Course course;
  final Function onTap;

  const MeCoursesListItem({this.course, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          heading: course.name,
          subHeading: course.semester,
          icon: course.type == 'owner'
              ? Icon(
                  Icons.perm_identity,
                  color: THMGrey,
                )
              : null,
        ));
  }
}
