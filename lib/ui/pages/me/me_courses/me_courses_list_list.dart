import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/ui/pages/me/me_courses/me_courses_list_item.dart';
import 'package:flutter/material.dart';

import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';

class MeCoursesList extends StatelessWidget {
  MeCoursesList({this.courses});

  final List<Course> courses;

  @override
  Widget build(BuildContext context) {
    return courses.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: courses.length,
              itemBuilder: (context, index) => MeCoursesListItem(
                course: courses[index],
                onTap: () {
                  Navigator.pushNamed(context, 'me/courses/detail',
                      arguments: courses[index]);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                "Keine Kurse gefunden",
                style: headline3,
                textAlign: TextAlign.center,
              ),
              UIHelper.verticalSpaceMedium(),
              FbsButton(
                  onPressed: () =>
                      Navigator.pushNamed(context, 'courses/create'),
                  text: "Kurs hinzufügen",
                  type: "primary",
                  size: "big")
            ],
          )));
  }
}
