import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_options_card.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class SessionDetailActionRow extends StatelessWidget {
  SessionDetailActionRow(this.session);

  final Session session;

  final user = Injector.get<AuthenticationService>().user;
  final comesFromCourse = Injector.get<SessionService>().comesFromCourse;

  createDeleteSessionDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Session löschen'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Willst du diese Session wirklich löschen?"),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr(
                  observe: () => RM.get<CoursesService>(),
                  onError: (error) => Text(
                    ErrorHandler.errorMessage(error),
                    style: TextStyle(color: Colors.red),
                  ),
                  builder: (_, __) => Container(),
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Nein', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              WhenRebuilderOr<SessionService>(
                observe: () => RM.get<SessionService>(),
                onWaiting: () => Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
                  ),
                ),
                builder: (_, sessionServiceRM) {
                  return Center(
                    child: FlatButton(
                      child: Text('Ja, löschen',
                          style: TextStyle(color: font_dark)),
                      onPressed: () {
                        sessionServiceRM.setState(
                          (state) =>
                              state.deleteSession(session, user.authToken),
                          onError: (context, error) =>
                              ErrorHandler.showErrorDialog(context, error),
                          onData: (context, sessionServiceRM) {
                            RM.get<SessionService>().refresh();
                            Navigator.pop(context); // closes dialog
                            Navigator.pop(context); //close sessionPage
                            if (comesFromCourse) {
                              Navigator.pushReplacementNamed(
                                  context, 'me/quizzes/detail',
                                  arguments: session.quiz);
                            } else {
                              Navigator.pushReplacementNamed(
                                  context, 'me/sessions');
                            }
                          },
                        );
                      },
                    ),
                  );
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FbsOptionsCard(
            onTap: () => Navigator.pushNamed(context, 'statistics/questions',
                arguments: this.session),
            iconData: Icons.poll,
            title: session.quiz.survey
                ? 'Session Statistiken'
                : 'Weitere Statistiken'),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => Navigator.pushNamed(context, 'session/edit',
                arguments: session),
            iconData: Icons.edit,
            title: 'Session bearbeiten'),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => createDeleteSessionDialog(context),
            iconData: Icons.delete,
            title: 'Session \nlöschen')
      ],
    );
  }
}
