import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/domain/exceptions/index.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:fbs_app/ui/widgets/main_app_bg.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class EditCoursePage extends StatefulWidget {
  EditCoursePage(this.course, {Key key}) : super(key: key);
  final TextEditingController title = new TextEditingController();
  final TextEditingController desc = new TextEditingController();
  final TextEditingController pw = new TextEditingController();
  final Course course;

  @override
  _EditCourse createState() => _EditCourse(title, desc, pw, course);
}

class _EditCourse extends State<EditCoursePage> {
  String semester = "Sommersemester";
  int year = int.parse(DateTime.now().year.toString());
  List<int> years = new List<int>();
  String chosenYear = DateTime.now().year.toString();

  final TextEditingController titleController;
  final TextEditingController descriptionController;
  final TextEditingController passwordController;
  final Course course;

  _EditCourse(this.titleController, this.descriptionController,
      this.passwordController, this.course) {
    if (course.id != null) {
      //course already exists: fill in all the controllers
      titleController.text = course.name;
      descriptionController.text = course.description;
      passwordController.text =
          course.password == null ? "" : course.password.toString();
      semester = course.semester.substring(0, 14);
      chosenYear = course.semester.substring(15, 19);
      year = int.parse(chosenYear);
    }
  }

  final user = Injector.get<AuthenticationService>().user;

  //return chosen semester as string formatted for backend
  String parseSemester() {
    String s = "";
    if (semester == "Sommersemester")
      s += "sose";
    else
      s += "wise";
    s += chosenYear;
    return s;
  }

  //check if all non-optional textfields have been filled in
  bool validate() {
    bool pwCheck = passwordController.text.trim().isNotEmpty
        ? int.tryParse(passwordController.text.trim()) != null
        : true;
    return (titleController.text.trim().isNotEmpty &&
        descriptionController.text.trim().isNotEmpty &&
        pwCheck);
  }

  Widget build(BuildContext context) {
    for (int i = year; i < year + 5; i++) {
      years.add(i);
    }
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: SingleChildScrollView(
              child: Stack(children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: MainAppBackground(
              headerHeight: (MediaQuery.of(context).size.height) / 3),
        ),
        SafeArea(
            child: SingleChildScrollView(
                child: Center(
                    child: Column(
          children: [
            Container(
                padding: EdgeInsets.all(25),
                child: Container(
                    padding: EdgeInsets.all(25),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    constraints: ((MediaQuery.of(context).size.width) > 768)
                        ? BoxConstraints(minWidth: 100, maxWidth: 500)
                        : null,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              constraints: ((MediaQuery.of(context).size.width -
                                          140) <
                                      272)
                                  ? BoxConstraints(
                                      maxWidth:
                                          (MediaQuery.of(context).size.width) -
                                              100 -
                                              40) //100 for padding, 40 for icon
                                  : null,
                              child: Text("Kurs bearbeiten",
                                  textAlign: TextAlign.left, style: headline2),
                            ),
                            Container(
                              child: InkResponse(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: CircleAvatar(
                                  backgroundColor: theme.focusColor,
                                  radius: 20,
                                  child: Icon(
                                    Icons.clear,
                                    color: Colors.white,
                                    //size: 20,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "Kursname",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 16,
                                color: font_dark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            child: TextField(
                              controller: titleController,
                              decoration: InputDecoration(
                                hintText: "Kursname eingeben",
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(
                                      color: green,
                                      style: BorderStyle.solid,
                                    )),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: green,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                              ),
                            )),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "Beschreibung",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 16,
                                color: font_dark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            constraints:
                                BoxConstraints(minHeight: 50, maxHeight: 100),
                            child: TextField(
                              textInputAction: TextInputAction.done,
                              controller: descriptionController,
                              textAlignVertical: TextAlignVertical.top,
                              maxLines: 5,
                              decoration: InputDecoration(
                                hintText: "Beschreibung eingeben",
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(
                                      color: green,
                                      style: BorderStyle.solid,
                                    )),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: green,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                              ),
                            )),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "Semester",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 16,
                                color: font_dark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            new DropdownButton<String>(
                              style: TextStyle(color: font_dark),
                              items: <String>[
                                'Sommersemester',
                                'Wintersemester'
                              ].map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(
                                    value,
                                    style: TextStyle(fontSize: 16),
                                  ),
                                );
                              }).toList(),
                              onChanged: (String newValue) {
                                setState(() {
                                  semester = newValue;
                                });
                              },
                              value: semester,
                            ),
                            new DropdownButton<String>(
                              style: TextStyle(color: font_dark),
                              items: <String>[
                                years[0].toString(),
                                years[1].toString(),
                                years[2].toString(),
                                years[3].toString(),
                                years[4].toString()
                              ].map((String value) {
                                return new DropdownMenuItem<String>(
                                  value: value,
                                  child: new Text(
                                    value,
                                    style: TextStyle(fontSize: 16),
                                  ),
                                );
                              }).toList(),
                              onChanged: (String newValue) {
                                setState(() {
                                  chosenYear = newValue;
                                });
                              },
                              value: chosenYear,
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: Text(
                            "Einschreibeschlüssel (optional)",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 16,
                                color: font_dark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Wrap(
                          children: [
                            Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                child: TextField(
                                  controller: passwordController,
                                  keyboardType: TextInputType.number,
                                  obscureText: false,
                                  decoration: InputDecoration(
                                    hintText: "Einschreibeschlüssel ändern",
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: green,
                                          style: BorderStyle.solid,
                                        )),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide: BorderSide(
                                        color: green,
                                        style: BorderStyle.solid,
                                      ),
                                    ),
                                  ),
                                )),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: WhenRebuilderOr<CoursesService>(
                            observe: () => RM.get<CoursesService>(),
                            onWaiting: () => Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    new AlwaysStoppedAnimation<Color>(THMGreen),
                              ),
                            ),
                            dispose: (_, __) {},
                            builder: (_, courseServiceRM) {
                              return Center(
                                child: FbsButton(
                                  text: "Bestätigen",
                                  type: "secondary",
                                  size: "big",
                                  onPressed: () {
                                    if (validate()) {
                                      Course c = new Course(
                                          id: course.id,
                                          name: titleController.text,
                                          description:
                                              descriptionController.text,
                                          semester: parseSemester(),
                                          password:
                                              passwordController.text.isNotEmpty
                                                  ? int.parse(
                                                      passwordController.text)
                                                  : null,
                                          protected: course.protected,
                                          type: course.type,
                                          docent: course.docent);
                                      courseServiceRM.setState(
                                          (state) => state.editCourse(
                                              c, user.authToken),
                                          onData: (context, courseServiceRM) {},
                                          onError: (context, e) {
                                        ErrorHandler.showErrorDialog(
                                            context, e);
                                      });
                                      Navigator.pushReplacementNamed(
                                          context, 'me/courses/detail',
                                          arguments: c);
                                    } else {
                                      ErrorHandler.showErrorDialog(
                                          context,
                                          ValidationException(
                                              "Bitte alle nötigen Felder ausfüllen."));
                                    }
                                  },
                                ),
                              );
                            },
                          ),
                        )
                      ],
                    ))),
          ],
        )))),
      ]))),
    );
  }
}
