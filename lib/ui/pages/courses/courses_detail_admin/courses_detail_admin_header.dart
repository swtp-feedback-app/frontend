import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import '../../../common/text_styles.dart';

class CoursesDetailAdminHeader extends StatelessWidget {
  CoursesDetailAdminHeader({this.pageTitle, this.course});

  final user = Injector.get<AuthenticationService>().user;
  final isOwner = Injector.get<CoursesService>().isOwner;

  final String pageTitle;
  final Course course;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      child: Container(
        transform: Matrix4.translationValues(0.0, 8.0, 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              //width: 100,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: CircleAvatar(
                  backgroundColor: bg_dark,
                  radius: 20,
                  child: Icon(Icons.arrow_back, color: Colors.white),
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width - 160,
                child: Text(
                  pageTitle,
                  style: headline3,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                )),
            InkWell(
              onTap: () {
                RM.get<CoursesService>().setState(
                  (state) {
                    if (state.isOwner == true) state.options = !state.options;
                  },
                );
              },
              child: CircleAvatar(
                backgroundColor: Colors.white,
                radius: 20,
                child: Icon(
                  Icons.settings,
                  color: bg_dark,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
