import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_options_card.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class CoursesDetailAdminOwnerActionRow extends StatelessWidget {
  CoursesDetailAdminOwnerActionRow(this.course);

  final Course course;
  final user = Injector.get<AuthenticationService>().user;

  createDeleteCourseDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Kurs löschen'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Willst du diesen Kurs wirklich löschen?"),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr(
                  observe: () => RM.get<CoursesService>(),
                  onError: (error) => Text(
                    ErrorHandler.errorMessage(error),
                    style: TextStyle(color: Colors.red),
                  ),
                  builder: (_, __) => Container(),
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Nein', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              WhenRebuilderOr<CoursesService>(
                observe: () => RM.get<CoursesService>(),
                onWaiting: () => Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
                  ),
                ),
                builder: (_, courseServiceRM) {
                  return Center(
                    child: FlatButton(
                      child: Text('Ja, löschen',
                          style: TextStyle(color: font_dark)),
                      onPressed: () {
                        courseServiceRM.setState(
                            (state) =>
                                state.deleteCourse(course, user.authToken),
                            onData: (context, courseServiceRM) {
                          Navigator.pop(context);
                          Navigator.pop(context);
                          Navigator.pushReplacementNamed(context, 'me/courses',
                                  arguments: course)
                              .then((result) {
                            Navigator.of(context)
                                .pop(); // Close dialog if user uses the backbutton on next screen
                          });
                        }, onError: (context, e) {
                          ErrorHandler.showErrorDialog(context, e);
                        });
                      },
                    ),
                  );
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FbsOptionsCard(
            onTap: () => Navigator.pushNamed(context, 'me/courses/edit',
                arguments: this.course),
            iconData: Icons.edit,
            title: 'Kurs bearbeiten'),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => Navigator.pushNamed(context, 'courses/detail/admins',
                arguments: course),
            iconData: Icons.perm_identity,
            title: 'Admins bearbeiten'),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => createDeleteCourseDialog(context),
            iconData: Icons.power_settings_new,
            title: 'Kurs \nlöschen')
      ],
    );
  }
}
