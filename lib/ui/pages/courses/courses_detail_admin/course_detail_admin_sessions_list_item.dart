import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';
import 'package:flutter/material.dart';

class CoursesDetailAdminSessionsListItem extends StatelessWidget {
  final Session session;
  final Function onTap;

  CoursesDetailAdminSessionsListItem({this.session, this.onTap});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          heading: session.quiz.name,
          subHeading: session.end * 1000 <
                  new DateTime.now().millisecondsSinceEpoch
              ? 'Geschlossen seit: ${session.getLocaleDatetime(session.end)}'
              : 'Geöffnet bis: ${session.getLocaleDatetime(session.end)}',
          icon: Icon(
            Icons.info_outline,
            color: THMGrey,
          ),
        ));
  }
}
