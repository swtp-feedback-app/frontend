import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/ui/pages/courses/courses_findCourses/courses_findCourses_item.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:fbs_app/domain/entities/token.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/service/courses_service.dart';

import '../../../common/app_colors.dart';
import '../../../common/app_theme.dart';
import '../../../common/text_styles.dart';
import '../../../common/ui_helpers.dart';
import '../../../exceptions/error_handler.dart';

class CourseFindCoursesList extends StatelessWidget {
  CourseFindCoursesList({this.courses, this.token});

  final List<Course> courses;
  final Token token;

  createSigninDialog(BuildContext context, Course course) {
    TextEditingController coursePassword = TextEditingController();

    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Kurspasswort'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                course.protected == true
                    ? TextField(
                        controller: coursePassword,
                        keyboardType: TextInputType.number,
                        onSubmitted: (term) {
                          RM.get<CoursesService>().setState(
                              (state) => state.joinCourse(
                                  course.id, coursePassword.text, token),
                              onData: (context, courseServiceRM) {
                            Navigator.pushReplacementNamed(
                                    context, 'courses/detail',
                                    arguments: course)
                                .then((result) {
                              Navigator.of(context)
                                  .pop(); // Close dialog if user uses the backbutton on next screen
                            });
                          }, onError: (context, e) {
                            ErrorHandler.showErrorDialog(context, e);
                          });
                        },
                        decoration: InputDecoration(
                          hintText: 'Beispiel: 1234',
                          border: new OutlineInputBorder(
                              borderRadius: new BorderRadius.circular(30.0),
                              borderSide: new BorderSide()),
                          focusedBorder: OutlineInputBorder(
                            borderSide: BorderSide(color: THMGreen, width: 2.0),
                            borderRadius: BorderRadius.circular(30.0),
                          ),
                        ),
                      )
                    : Text("Dieser Kurs benötigt kein Passwort"),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr(
                  observe: () => RM.get<CoursesService>(),
                  onError: (error) => Text(
                    ErrorHandler.errorMessage(error),
                    style: TextStyle(color: Colors.red),
                  ),
                  builder: (_, __) => Container(),
                ),
              ],
            ),
            actions: <Widget>[
              WhenRebuilderOr<CoursesService>(
                observe: () => RM.get<CoursesService>(),
                onWaiting: () => Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
                  ),
                ),
                dispose: (_, __) {
                  coursePassword.dispose();
                },
                builder: (_, courseServiceRM) {
                  return Center(
                    child: FlatButton(
                      child: Text('Einschreiben',
                          style: TextStyle(color: font_dark)),
                      onPressed: () {
                        courseServiceRM.setState(
                            (state) => state.joinCourse(
                                course.id, coursePassword.text, token),
                            onData: (context, courseServiceRM) {
                          Navigator.of(context)
                            ..pop()
                            ..pop()
                            ..pushReplacementNamed('courses')
                            ..pushNamed('courses/detail', arguments: course);
                        }, onError: (context, e) {
                          ErrorHandler.showErrorDialog(context, e);
                        });
                      },
                    ),
                  );
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return courses.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              padding: EdgeInsets.only(top: 8),
              shrinkWrap: true,
              itemCount: courses.length,
              itemBuilder: (context, index) => CourseFindCoursesListItem(
                course: courses[index],
                onTap: () {
                  createSigninDialog(context, courses[index]);
                },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Text(
            "Zu deiner Suche wurden keine Kurse gefunden",
            style: headline3,
            textAlign: TextAlign.center,
          )));
  }
}
