import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/pages/courses/courses_findCourses/course_findCourses_list.dart';
import 'package:fbs_app/ui/widgets/fbs_action_button.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/widgets/fbs_searchfield.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';

class CoursesFindCoursesPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final TextEditingController searchController = new TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [Inject(() => CoursesService(api: Injector.get()))],
        builder: (context) {
          return GestureDetector(
            onTap: () => FocusScope.of(context).unfocus(),
            child: Scaffold(
              appBar: FBSAppBar(
                  leading: FbsActionButton(type: 'close'),
                  title: 'Kurs beitreten'),
              body: Center(
                child: Container(
                  constraints:
                      BoxConstraints(maxWidth: 850), // limit the width for Web
                  child: Padding(
                    padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                    child: Column(
                      children: <Widget>[
                        Column(
                          children: <Widget>[
                            UIHelper.verticalSpaceSmall(),
                            FbsSearchField(
                              onChanged: (value) {
                                RM.get<CoursesService>().setState(
                                      (state) => state
                                          .getAllCoursesForUserFilter(value),
                                    );
                              },
                              textController: searchController,
                            )
                          ],
                        ),
                        WhenRebuilderOr<CoursesService>(
                          observe: () => RM.get<CoursesService>(),
                          initState: (_, coursesServiceRM) {
                            coursesServiceRM.setState(
                                (state) => state.getAllCourses(user.authToken),
                                onError: (context, e) {
                              ErrorHandler.showErrorDialog(context, e);
                            });
                          },
                          onWaiting: () {
                            return Expanded(
                              child: Center(
                                child: CircularProgressIndicator(
                                  valueColor: new AlwaysStoppedAnimation<Color>(
                                      THMGreen),
                                ),
                              ),
                            );
                          },
                          onError: (error) {
                            return Expanded(
                              child: Center(
                                child: Text(ErrorHandler.errorMessage(error),
                                    textAlign: TextAlign.center),
                              ),
                            );
                          },
                          builder: (_, coursesService) {
                            return CourseFindCoursesList(
                                courses: coursesService.state.allCourses,
                                token: user.authToken);
                          },
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                  ? FbsBottomNavbar(index: 0)
                  : null,
            ),
          );
        });
  }
}
