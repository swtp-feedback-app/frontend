import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_options_card.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class CoursesDetailActionRow extends StatelessWidget {
  CoursesDetailActionRow(this.course);

  final Course course;

  final user = Injector.get<AuthenticationService>().user;

  createSignoutDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Ausschreiben'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Willst du dich wirklich aus diesem Kurs auschreiben?"),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr(
                  observe: () => RM.get<CoursesService>(),
                  onError: (error) => Text(
                    ErrorHandler.errorMessage(error),
                    style: TextStyle(color: Colors.red),
                  ),
                  builder: (_, __) => Container(),
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Nein', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              WhenRebuilderOr<CoursesService>(
                observe: () => RM.get<CoursesService>(),
                onWaiting: () => Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
                  ),
                ),
                builder: (_, courseServiceRM) {
                  return Center(
                    child: FlatButton(
                      child: Text('Ja, ausschreiben',
                          style: TextStyle(color: font_dark)),
                      onPressed: () {
                        courseServiceRM.setState(
                            (state) =>
                                state.leaveCourse(course.id, user.authToken),
                            onData: (context, courseServiceRM) {
                          Navigator.of(context)
                            ..pop()
                            ..pop()
                            ..popAndPushNamed('courses',
                                arguments:
                                    course); // Close dialog if user uses the backbutton on next screen
                        }, onError: (context, e) {
                          ErrorHandler.showErrorDialog(context, e);
                        });
                      },
                    ),
                  );
                },
              ),
            ],
          );
        });
  }

  createInfoDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Kursinformationen', style: headline4),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text("Kursname:", style: headline5),
                Text(course.name, style: headline6),
                SizedBox(
                  height: 5,
                ),
                Text("Kursbeschreibung:", style: headline5),
                Text(course.description, style: headline6),
                SizedBox(
                  height: 5,
                ),
                Text("Dozent:", style: headline5),
                Container(
                  child: Text(
                    course.docent.toString(),
                    style: headline6,
                    //softWrap: false,
                  ),
                ),
                SizedBox(
                  height: 5,
                ),
                Text("Semester:", style: headline5),
                Text(course.semester, style: headline6),
                SizedBox(
                  height: 5,
                ),
                Text("Kurs-ID:", style: headline5),
                Text(course.id.toString(), style: headline6),
                SizedBox(
                  height: 5,
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Alles klar!', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  createStatsDialog(BuildContext context) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Kursstatistiken'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text("Hier kannst du in Kürze deine Kursstatistiken einsehen"),
                UIHelper.verticalSpaceSmall(),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Alles klar!', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        FbsOptionsCard(
            onTap: () => Navigator.pushNamed(
                context, 'courses/detail/statistics',
                arguments: course),
            // onTap: () => createStatsDialog(context),
            iconData: Icons.insert_chart,
            title: 'Meine Statistiken'),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => createInfoDialog(context),
            iconData: Icons.info,
            title: 'Kurs \nInfos'),
        SizedBox(width: 20),
        FbsOptionsCard(
            onTap: () => createSignoutDialog(context),
            iconData: Icons.exit_to_app,
            title: 'Kurs verlassen'),
      ],
    );
  }
}
