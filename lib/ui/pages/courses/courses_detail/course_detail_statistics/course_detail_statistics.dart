import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/service/statistics_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';
import 'package:pie_chart/pie_chart.dart';

class CourseDetailStatisticsPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final Course course;

  CourseDetailStatisticsPage({this.course});

  @override
  Widget build(BuildContext context) {
    return Injector(
        inject: [
          Inject(() => UserCourseStatisticsService(api: Injector.get()))
        ],
        builder: (context) {
          return Scaffold(
            appBar: FBSAppBar(
              title: course.name,
            ),
            body: Align(
              alignment: Alignment.topCenter,
              child: Container(
                constraints:
                    BoxConstraints(maxWidth: 850), // limit the width for Web
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                  child: SingleChildScrollView(
                      child: Center(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        UIHelper.verticalSpaceMedium(),
                        Container(
                          alignment: Alignment.centerLeft,
                          child: Text(
                            'Statistiken',
                            textAlign: TextAlign.start,
                            style: headline4,
                          ),
                        ),
                        UIHelper.verticalSpaceSmall(),
                        WhenRebuilderOr<UserCourseStatisticsService>(
                          observe: () => RM.get<UserCourseStatisticsService>(),
                          initState: (_, userCourseStatisticsServiceRM) {
                            userCourseStatisticsServiceRM.setState(
                                (state) => state.getUserCourseStats(
                                    user.authToken, course.id),
                                onError: (context, e) {
                              ErrorHandler.showErrorDialog(context, e);
                            });
                          },
                          onWaiting: () {
                            return Container();
                          },
                          onError: (error) => Center(
                              child: Text(ErrorHandler.errorMessage(error),
                                  style: headline5)),
                          builder: (_, userCourseStatisticsService) {
                            return Container(
                              padding: EdgeInsets.all(20),
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(12),
                                boxShadow: [
                                  BoxShadow(
                                    color: Colors.grey.withOpacity(0.1),
                                    spreadRadius: 3,
                                    blurRadius: 5,
                                    offset: Offset(0, 5),
                                  ),
                                ],
                              ),
                              child: userCourseStatisticsService
                                          .state.stats.answQuestions >
                                      0
                                  ? Column(
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          child: Text(
                                            'Beantwortete Fragen',
                                            style: headline5,
                                            textAlign: TextAlign.left,
                                          ),
                                        ),
                                        UIHelper.verticalSpaceSmall(),
                                        PieChart(
                                          dataMap: userCourseStatisticsService
                                              .state.stats.chartData,
                                          colorList: [
                                            THMGreen,
                                            Colors.red[300]
                                          ],
                                          legendStyle: TextStyle(
                                              color: font_dark,
                                              fontWeight: FontWeight.bold,
                                              fontSize: 12),
                                          showChartValuesInPercentage: true,
                                          legendPosition: LegendPosition.right,
                                        ),
                                        UIHelper.verticalSpaceSmall(),
                                        Column(
                                          children: [
                                            RichText(
                                              text: TextSpan(
                                                text: 'Bearbeitete Sessions: ',
                                                style: headline5,
                                                children: <TextSpan>[
                                                  TextSpan(
                                                      text:
                                                          userCourseStatisticsService
                                                              .state
                                                              .stats
                                                              .sessions
                                                              .toString(),
                                                      style: null),
                                                ],
                                              ),
                                            ),
                                          ],
                                        ),
                                      ],
                                    )
                                  : Text(
                                      "Es wurde noch keine Session abgeschlossen."),
                            );
                          },
                        ),
                        UIHelper.verticalSpaceMedium(),
                      ],
                    ),
                  )),
                ),
              ),
            ),
            bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                ? FbsBottomNavbar(index: 0)
                : null,
          );
        });
  }
}
