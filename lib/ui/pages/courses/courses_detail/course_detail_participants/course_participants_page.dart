import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/pages/courses/courses_detail/course_detail_participants/course_participants_list.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';

import 'package:fbs_app/ui/common/ui_helpers.dart';

class CoursesParticipantsPage extends StatelessWidget {
  final user = Injector.get<AuthenticationService>().user;
  final Course course;
  final bool admin;
  final TextEditingController searchController = new TextEditingController();

  CoursesParticipantsPage({this.course, this.admin});

  @override
  Widget build(BuildContext context) {
    var text;
    if (admin) {
      text = 'Admins';
    } else {
      text = 'Teilnehmer';
    }
    return Injector(
        inject: [Inject(() => CoursesService(api: Injector.get()))],
        builder: (context) {
          return Scaffold(
            appBar: FBSAppBar(
              title: text,
              action: admin
                  ? InkWell(
                      onTap: () {
                        Navigator.pushNamed(
                            context, 'courses/detail/participants',
                            arguments: course);
                      },
                      child: CircleAvatar(
                        backgroundColor: Colors.white,
                        radius: 20,
                        child: Icon(
                          Icons.person_add,
                          color: bg_dark,
                        ),
                      ),
                    )
                  : null,
            ),
            body: Center(
              child: Container(
                constraints:
                    BoxConstraints(maxWidth: 850), // limit the width for Web
                child: Padding(
                  padding: const EdgeInsets.fromLTRB(24, 0, 24, 0),
                  child: Column(
                    children: <Widget>[
                      UIHelper.verticalSpaceSmall(),
                      WhenRebuilderOr<CoursesService>(
                        observe: () => RM.get<CoursesService>(),
                        initState: (_, coursesServiceRM) {
                          coursesServiceRM.setState(
                              (state) => admin
                                  ? state.getCourseAdmins(
                                      course.id, user.authToken)
                                  : state.getCourseParticipants(
                                      course.id, user.authToken,
                                      hideAdmins: true), onError: (context, e) {
                            ErrorHandler.showErrorDialog(context, e);
                          });
                        },
                        onWaiting: () {
                          return Expanded(
                            child: Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    new AlwaysStoppedAnimation<Color>(THMGreen),
                              ),
                            ),
                          );
                        },
                        builder: (_, coursesService) {
                          return CourseParticipantsList(
                              users: admin
                                  ? coursesService.state.admins
                                  : coursesService.state.participants,
                              admin: admin,
                              course: course);
                        },
                      ),
                    ],
                  ),
                ),
              ),
            ),
            bottomNavigationBar: ((MediaQuery.of(context).size.width) < 768)
                ? FbsBottomNavbar(index: 2)
                : null,
          );
        });
  }
}
