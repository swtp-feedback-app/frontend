import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/domain/entities/token.dart';
import 'package:fbs_app/domain/entities/user.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/courses_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:fbs_app/ui/widgets/fbs_list_card.dart';

class CourseParticipantsList extends StatelessWidget {
  CourseParticipantsList({this.users, this.admin, this.course});

  final bool admin;
  final Course course;
  final List<User> users;

  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    return users.isNotEmpty
        ? Expanded(
            child: ListView.builder(
              shrinkWrap: true,
              itemCount: users.length,
              itemBuilder: (context, index) => ParticipantsListItem(
                participant: users[index],
                admin: admin,
                onTap: admin
                    ? () => createDeleteAdminDialog(
                        context, course, users[index], user.authToken)
                    : () {
                        RM.get<CoursesService>().setState(
                            (state) => state.addCourseAdmin(
                                course.id, users[index], user.authToken),
                            onData: (context, courseServiceRM) {
                          Navigator.pop(context);
                          Navigator.pushReplacementNamed(
                              context, 'courses/detail/admins',
                              arguments: course);
                        }, onError: (context, e) {
                          ErrorHandler.showErrorDialog(context, e);
                        });
                      },
              ),
            ),
          )
        : Expanded(
            child: Center(
                child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              admin
                  ? Text(
                      "Dieser Kurs hat noch keinen Admin.",
                      style: headline3,
                      textAlign: TextAlign.center,
                    )
                  : Text(
                      "Keine Teilnehmer gefunden.",
                      style: headline3,
                      textAlign: TextAlign.center,
                    ),
              UIHelper.verticalSpaceMedium(),
              admin
                  ? FbsButton(
                  onPressed: () =>
                      Navigator.pushNamed(
                          context, 'courses/detail/participants',
                          arguments: course),
                      text: "Admin hinzufügen",
                      type: "primary",
                      size: "big")
                  : Container()
            ],
          )));
  }

  createDeleteAdminDialog(
      BuildContext context, Course course, User user, Token authToken) {
    var username = user.username;
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.all(Radius.circular(12.0))),
            title: Text('Admin entfernen'),
            content: Column(
              mainAxisSize: MainAxisSize.min,
              children: [
                Text(
                    "Willst du Benutzer $username wirklich als Admin entfernen?"),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr(
                  observe: () => RM.get<CoursesService>(),
                  onError: (error) => Text(
                    ErrorHandler.errorMessage(error),
                    style: TextStyle(color: Colors.red),
                  ),
                  builder: (_, __) => Container(),
                ),
              ],
            ),
            actions: <Widget>[
              FlatButton(
                child: Text('Nein', style: TextStyle(color: font_dark)),
                onPressed: () {
                  Navigator.of(context).pop();
                },
              ),
              WhenRebuilderOr<CoursesService>(
                observe: () => RM.get<CoursesService>(),
                onWaiting: () => Center(
                  child: CircularProgressIndicator(
                    valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
                  ),
                ),
                builder: (_, courseServiceRM) {
                  return Center(
                    child: FlatButton(
                      child: Text('Ja, entfernen',
                          style: TextStyle(color: font_dark)),
                      onPressed: () {
                        RM.get<CoursesService>().setState(
                            (state) => state.deleteCourseAdmin(
                                course.id, user, authToken),
                            onData: (context, courseServiceRM) {
                              Navigator.pop(context);
                              Navigator.pushReplacementNamed(
                              context, 'courses/detail/admins',
                              arguments: course);
                        }, onError: (context, e) {
                          ErrorHandler.showErrorDialog(context, e);
                        });
                      },
                    ),
                  );
                },
              ),
            ],
          );
        });
  }
}

class ParticipantsListItem extends StatelessWidget {
  final User participant;
  final Function onTap;
  final bool admin;

  ParticipantsListItem({this.participant, this.onTap, this.admin});

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: onTap,
        child: FbsListCard(
          subHeading: participant.username,
          heading: participant.firstname == null
              ? ""
              : participant.firstname + ' ' + participant.lastname,
          icon: admin
              ? Icon(
                  Icons.delete,
                  color: red,
                )
              : Icon(
                  Icons.add,
                  color: THMGreen,
                ),
        ));
  }
}
