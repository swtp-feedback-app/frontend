import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:flutter/material.dart';

import '../../../../common/text_styles.dart';

class CoursesParticipantsHeader extends StatelessWidget {
  CoursesParticipantsHeader({this.pageTitle, this.onTap, this.add});

  final Function onTap;
  final bool add;
  final String pageTitle;

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 90,
      //width: MediaQuery.of(context).size.width,
      child: Container(
        transform: Matrix4.translationValues(0.0, 8.0, 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Container(
              //width: 100,
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: CircleAvatar(
                  backgroundColor: bg_dark,
                  radius: 20,
                  child: Icon(Icons.arrow_back, color: Colors.white),
                ),
              ),
            ),
            Container(
                width: MediaQuery.of(context).size.width - 160,
                child: Text(
                  pageTitle,
                  style: headline3,
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                )),
            add
                ? InkWell(
                    onTap: onTap,
                    child: CircleAvatar(
                      backgroundColor: Colors.white,
                      radius: 20,
                      child: Icon(
                        Icons.person_add,
                        color: bg_dark,
                      ),
                    ),
                  )
                : Container()
          ],
        ),
      ),
    );
  }
}
