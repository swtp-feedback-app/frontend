import 'package:fbs_app/domain/entities/user.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/pages/login_page/login_session_input.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flip_card/flip_card.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

import '../../common/app_colors.dart';
import 'package:fbs_app/ui/pages/login_page/login_credentials_input.dart';

const version = 'Version 2.4';
const appName = 'Feedback App';
const appLegalese = 'SoSe2020 Softwaretechnik Projekt';

class LoginPage extends StatefulWidget {
  @override
  _LoginPageState createState() => _LoginPageState();
}

class _LoginPageState extends State<LoginPage> {
  GlobalKey<FlipCardState> cardKey = GlobalKey<FlipCardState>();
  bool isFront = true; //which side of flipcard is shown
  bool flip; //flip card or underneath
  User user;
  bool loggedIn = Injector.get<AuthenticationService>().loggedIn;

  //on first load: check if authToken in local storage exists
  @override
  void initState() {
    super.initState();
    if (!kIsWeb && loggedIn) {
      Injector.get<AuthenticationService>()
          .checkForToken()
          .catchError((error) => ErrorHandler.showErrorDialog(context, error))
          .whenComplete(() {
        setState(() {
          this.loggedIn = Injector.get<AuthenticationService>().loggedIn;
          this.user = Injector.get<AuthenticationService>().user;
        });
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    final List<Widget> aboutBoxChildren = <Widget>[
      SizedBox(
        height: 20,
      ),
      Text('Die FeedbackApp wird im Rahmen des Moduls Softwaretechnik-Projekt '
          'erstellt. Ziel ist eine App, mit welcher Umfragen und Quizze im Rahmen '
          'von Lehrveranstaltungen durchgeführt werden können.\n'),
      new GestureDetector(
        onTap: () {
          Navigator.pushNamed(context, "impressum");
        },
        child: new Text(
          "Impressum und Datenschutz",
          style: TextStyle(
            color: Colors.blue,
          ),
        ),
      ),
    ];
    flip = (MediaQuery.of(context).size.width < 768);
    if (user != null) {
      Future(() {
        Navigator.pushReplacementNamed(context, 'home');
      });
    }
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: (MediaQuery.of(context).size.width < 768)
              ? Container(
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
                  child: (!kIsWeb && loggedIn)
                      ? Container(
                          child: Center(
                              child: Text('Die App wird gestartet...',
                                  style: TextStyle(
                                      color: font_dark, fontSize: 16))),
                        )
                      : SingleChildScrollView(
                          child: Stack(children: <Widget>[
                          Column(//background
                              children: <Widget>[
                            Container(
                              height: MediaQuery.of(context).size.height *
                                  (20 / 100),
                              width: MediaQuery.of(context).size.width,
                              color: bg_light,
                            ),
                            Container(
                                height: MediaQuery.of(context).size.height *
                                    (60 / 100),
                                width: MediaQuery.of(context).size.width,
                                color: bg_dark,
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: [],
                                )),
                            Container(
                              height: MediaQuery.of(context).size.height *
                                  (20 / 100),
                              width: MediaQuery.of(context).size.width,
                              color: bg_green,
                            ),
                          ]),
                          SafeArea(
                              child: Container(
                                  constraints: (BoxConstraints(
                                      minWidth: 100, maxWidth: 500)),
                                  height: MediaQuery.of(context).size.height -
                                      50, //for padding
                                  child: Column(
                                      mainAxisAlignment:
                                          MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                          CrossAxisAlignment.center,
                                      children: <Widget>[
                                        Container(
                                          padding:
                                              EdgeInsets.fromLTRB(0, 10, 0, 0),
                                          constraints: BoxConstraints(
                                              minWidth: 200, maxWidth: 250),
                                          child: Image.asset(
                                            "assets/images/THM_Logo.png",
                                          ),
                                        ),
                                        FlipCard(
                                          key: cardKey,
                                          onFlipDone: (status) {
                                            setState(() {
                                              isFront = !isFront;
                                            });
                                          },
                                          direction: FlipDirection.HORIZONTAL,
                                          // default
                                          front: LoginSessionInput(
                                              sessionController:
                                                  new TextEditingController()),
                                          back: LoginCredentialsInputState(
                                            idController:
                                                new TextEditingController(),
                                            passwordController:
                                                new TextEditingController(),
                                          ),
                                        ),
                                        Container(),
                                        // needed for spaceBetween - maybe refactor
                                      ]))),
                          Container(
                            height: MediaQuery.of(context).size.height,
                            padding: EdgeInsets.fromLTRB(0, 0, 0, 25),
                            child: Stack(children: <Widget>[
                              Container(
                                alignment: Alignment.bottomLeft,
                                width: MediaQuery.of(context).size.width - 110,
                                padding: EdgeInsets.only(left: 15),
                                child: FbsButton(
                                    onPressed: () =>
                                        cardKey.currentState.toggleCard(),
                                    text: isFront == true
                                        ? 'Oder melde dich an'
                                        : 'Zurück zum Start',
                                    type: 'secondary',
                                    size: 'big'),
                              ),
                              Container(
                                alignment: Alignment.bottomRight,
                                child: RawMaterialButton(
                                  onPressed: () {
                                    showAboutDialog(
                                        context: context,
                                        applicationName: appName,
                                        applicationVersion: version,
                                        applicationLegalese: appLegalese,
                                        children: aboutBoxChildren);
                                  },
                                  elevation: 2.0,
                                  fillColor: green,
                                  child: Text(
                                    "?",
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontWeight: FontWeight.bold,
                                        fontSize: 25),
                                  ),
                                  padding: EdgeInsets.all(15.0),
                                  shape: CircleBorder(),
                                ),
                              ),
                            ]),
                          ),
                        ])) //;
                  )
              : _buildWeb(context, aboutBoxChildren)),
    );
  }

  Widget _buildWeb(BuildContext context, List<Widget> aboutBoxChildren) {
    final small = MediaQuery.of(context).size.width < 768;
    return Scaffold(
        body: SafeArea(
            child: SingleChildScrollView(
                child: Container(
                    color: bg_green,
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height,
                    child: SingleChildScrollView(
                      child: Stack(
                        children: <Widget>[
                          Column(//background
                              children: <Widget>[
                            Container(
                              height: 200,
                              width: MediaQuery.of(context).size.width,
                              color: bg_light,
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height / 2,
                              width: MediaQuery.of(context).size.width,
                              color: bg_dark,
                            ),
                            Container(
                              height: MediaQuery.of(context).size.height / 4,
                              width: MediaQuery.of(context).size.width,
                              color: bg_green,
                            ),
                          ]),
                          //background-fin
                          Container(
                              padding: EdgeInsets.fromLTRB(
                                  0,
                                  MediaQuery.of(context).size.height / 20,
                                  0,
                                  0),
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              child: SingleChildScrollView(
                                child: Column(
                                  mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                  crossAxisAlignment: CrossAxisAlignment.center,
                                  children: <Widget>[
                                    Container(
                                      child: Container(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 25, 0, 0),
                                        constraints: BoxConstraints(
                                            minWidth: 200, maxWidth: 350),
                                        child: Image.asset(
                                          "assets/images/THM_Logo.png",
                                        ),
                                      ),
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                                      constraints: !small
                                          ? BoxConstraints(
                                              minWidth: 100, maxWidth: 500)
                                          : null,
                                      child: LoginSessionInput(
                                          sessionController:
                                              new TextEditingController()),
                                    ),
                                    Container(
                                      padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                                      constraints: !small
                                          ? BoxConstraints(
                                              minWidth: 100, maxWidth: 500)
                                          : null,
                                      child: LoginCredentialsInputState(
                                          idController:
                                              new TextEditingController(),
                                          passwordController:
                                              new TextEditingController()),
                                    ),
                                    Container(
                                        padding:
                                            EdgeInsets.fromLTRB(0, 0, 0, 35),
                                        child: Align(
                                          alignment: Alignment.bottomRight,
                                          child: RawMaterialButton(
                                            onPressed: () {
                                              showAboutDialog(
                                                  context: context,
                                                  applicationName: appName,
                                                  applicationVersion: version,
                                                  applicationLegalese:
                                                      appLegalese,
                                                  children: aboutBoxChildren);
                                            },
                                            elevation: 2.0,
                                            fillColor: (bg_dark),
                                            child: Text(
                                              "?",
                                              style: TextStyle(
                                                  color: Colors.white,
                                                  fontWeight: FontWeight.bold,
                                                  fontSize: 25),
                                            ),
                                            padding: EdgeInsets.all(15.0),
                                            shape: CircleBorder(),
                                          ),
                                        ))
                                  ],
                                ),
                              ))
                        ],
                      ),
                    )))));
  }
}
