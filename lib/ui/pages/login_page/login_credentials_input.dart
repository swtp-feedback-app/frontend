import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class LoginCredentialsInputState extends StatefulWidget {
  final TextEditingController idController;
  final TextEditingController passwordController;

  LoginCredentialsInputState({this.passwordController, this.idController});

  @override
  LoginCredentialsInput createState() => LoginCredentialsInput(
      idController: this.idController,
      passwordController: this.passwordController);
}

class LoginCredentialsInput extends State<LoginCredentialsInputState> {
  final TextEditingController idController;
  final TextEditingController passwordController;
  final FocusNode _inputIdFocus = FocusNode();
  final FocusNode _inputPwFocus = FocusNode();

  LoginCredentialsInput(
      {@required this.idController, @required this.passwordController});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24.0),
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(12.0),
        ),
        child: Padding(
            padding: EdgeInsets.all(25),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  padding: EdgeInsets.fromLTRB(0, 0, 0, 10),
                  child: Text(
                    'Melde dich hier an:',
                    textAlign: TextAlign.start,
                    style: headline4,
                  ),
                ),
                Container(
                  child: TextFormField(
                    controller: idController,
                    cursorColor: THMGreen,
                    showCursor: false,
                    textInputAction: TextInputAction.next,
                    focusNode: _inputIdFocus,
                    onFieldSubmitted: (term) {
                      _inputIdFocus.unfocus();
                      FocusScope.of(context).requestFocus(_inputPwFocus);
                    },
                    decoration: new InputDecoration(
                      hintText: 'THM-ID',
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                          borderSide: new BorderSide()),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: THMGreen, width: 2.0),
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                  ),
                ),
                UIHelper.verticalSpaceSmall(),
                Container(
                  child: TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    cursorColor: THMGreen,
                    showCursor: false,
                    textInputAction: TextInputAction.done,
                    focusNode: _inputPwFocus,
                    onFieldSubmitted: (term) {
                      RM.get<AuthenticationService>().setState(
                        (state) => state.login(
                            idController.text.trim(), passwordController.text),
                        onData: (context, authServiceRM) {
                          if (authServiceRM.user != null) {
                            Navigator.pushReplacementNamed(context, 'home');
                          }
                        },
                      );
                    },
                    decoration: new InputDecoration(
                      hintText: 'Passwort',
                      border: new OutlineInputBorder(
                          borderRadius: new BorderRadius.circular(30.0),
                          borderSide: new BorderSide()),
                      focusedBorder: OutlineInputBorder(
                        borderSide: BorderSide(color: THMGreen, width: 2.0),
                        borderRadius: BorderRadius.circular(30.0),
                      ),
                    ),
                  ),
                ),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr(
                  observe: () => RM.get<AuthenticationService>(),
                  onError: (error) => Text(
                    ErrorHandler.errorMessage(error),
                    style: TextStyle(color: Colors.red),
                  ),
                  builder: (_, __) => Container(),
                ),
                UIHelper.verticalSpaceSmall(),
                WhenRebuilderOr<AuthenticationService>(
                  observe: () => RM.get<AuthenticationService>(),
                  onWaiting: () => Center(
                    child: CircularProgressIndicator(
                      valueColor: new AlwaysStoppedAnimation<Color>(THMGreen),
                    ),
                  ),
                  dispose: (_, __) {
                    idController.dispose();
                    passwordController.dispose();
                  },
                  builder: (_, authServiceRM) {
                    return Center(
                      child: FbsButton(
                        text: "Login",
                        type: "primary",
                        size: "big",
                        onPressed: () {
                          authServiceRM.setState(
                            (state) => state.login(
                                idController.text.trim(), passwordController.text),
                            onData: (context, authServiceRM) {
                              if (authServiceRM.user != null) {
                                Navigator.pushReplacementNamed(context, 'home');
                              }
                            },
                          );
                        },
                      ),
                    );
                  },
                ),
              ],
            )),
      ),
    );
  }
}
