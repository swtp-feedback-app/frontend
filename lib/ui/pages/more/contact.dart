import 'package:fbs_app/domain/exceptions/validation_exception.dart';
import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/ui/common/ui_helpers.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_appbar.dart';
import 'package:fbs_app/ui/widgets/fbs_bottom_navbar.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:states_rebuilder/states_rebuilder.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:flutter/foundation.dart' show kIsWeb;

class ContactPage extends StatefulWidget {
  ContactPage({Key key}) : super(key: key);

  @override
  State<StatefulWidget> createState() => _Contact();
}

class _Contact extends State<ContactPage> {
  final _textEditingController = new TextEditingController();
  final user = Injector.get<AuthenticationService>().user;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    final text1 =
        "Du hast ein Frage oder möchtest das Fehlverhalten eines anderen Benutzers melden?";
    final text2a =
        "Dann schreibe dein Anliegen in das Textfeld und klicke auf 'Weiter',\noder sende uns direkt eine Email an: ";
    final text2b = "Dann schreibe uns eine Email mit deinem Anliegen an: ";
    final text3 = "FeedbackApp@protonmail.com";
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
        appBar: FBSAppBar(title: "Kontakt"),
        body: SingleChildScrollView(
          child: SafeArea(
            child: Center(
              child: Container(
                constraints: ((screenSize.width) > 768)
                    ? BoxConstraints(minWidth: 100, maxWidth: 500)
                    : null,
                padding: EdgeInsets.fromLTRB(24, 24, 24, 0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      text1,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: 20.0,
                          fontWeight: FontWeight.w500,
                          color: greyLetters),
                    ),
                    UIHelper.verticalSpaceMedium(),
                    Text(kIsWeb ? text2b : text2a,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16.0,
                            fontWeight: FontWeight.w500,
                            color: greyLetters)),
                    UIHelper.verticalSpaceSmall(),
                    Text(text3,
                        textAlign: TextAlign.center,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: green)),
                    UIHelper.verticalSpaceMedium(),
                    kIsWeb
                        ? Container()
                        : Column(
                            children: [
                              UIHelper.verticalSpaceSmall(),
                              Text("Dein Anliegen:",
                                  style: TextStyle(
                                      fontSize: 16.0,
                                      fontWeight: FontWeight.w500,
                                      color: greyLetters)),
                              UIHelper.verticalSpaceSmall(),
                              Container(
                                constraints: BoxConstraints(
                                    maxHeight: screenSize.height * 0.3,
                                    minHeight: screenSize.height * 0.1),
                                decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(12),
                                    boxShadow: [
                                      BoxShadow(
                                        color: Colors.grey.withOpacity(0.2),
                                        spreadRadius: 3,
                                        blurRadius: 5,
                                        offset: Offset(0, 5),
                                      ),
                                    ]),
                                child: TextField(
                                  expands: true,
                                  autofocus: false,
                                  textAlignVertical: TextAlignVertical.top,
                                  cursorColor: green,
                                  textInputAction: TextInputAction.done,
                                  maxLines: null,
                                  decoration: InputDecoration(
                                    focusedBorder: OutlineInputBorder(
                                        borderRadius:
                                            BorderRadius.circular(5.0),
                                        borderSide: BorderSide(
                                          color: green,
                                          style: BorderStyle.solid,
                                        )),
                                    border: OutlineInputBorder(
                                      borderRadius: BorderRadius.circular(5.0),
                                      borderSide: BorderSide(
                                        color: green,
                                        style: BorderStyle.solid,
                                      ),
                                    ),
                                  ),
                                  controller: _textEditingController,
                                ),
                              ),
                            ],
                          ),
                    UIHelper.verticalSpaceMedium(),
                    kIsWeb
                        ? Container()
                        : Center(
                            child: FbsButton(
                            onPressed: () => _validate(context),
                            size: 'big',
                            type: 'primary',
                            text: 'Weiter',
                          )),
                    UIHelper.verticalSpaceMedium(),
                  ],
                ),
              ),
            ),
          ),
        ),
        bottomNavigationBar: ((MediaQuery.of(context).size.width) < 850)
            ? FbsBottomNavbar(index: 2)
            : null,
      ),
    );
  }

  _send(String text, BuildContext context) async {
    var url = Uri.encodeFull(
        'mailto:FeedbackApp@protonmail.com?subject=Anliegen Feedback App&body=$text');
    if (await canLaunch(url)) {
      await launch(url);
    } else {
      ErrorHandler.showErrorDialog(
          context,
          ValidationException(
              "Du hast keine App zum Email versenden installiert, schreib uns bitte manuell eine Email."));
    }
  }

  _validate(BuildContext context) async {
    var text = _textEditingController.text.trim();
    if (text == "") {
      ErrorHandler.showErrorDialog(
          context, ValidationException("Du musst das Textfeld erst ausfüllen"));
    } else {
      _send(text, context);
    }
  }
}
