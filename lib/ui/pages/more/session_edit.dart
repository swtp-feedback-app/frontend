import 'package:fbs_app/service/authentication_service.dart';
import 'package:fbs_app/service/session_service.dart';
import 'package:fbs_app/ui/common/app_colors.dart';
import 'package:fbs_app/ui/common/text_styles.dart';
import 'package:fbs_app/ui/exceptions/error_handler.dart';
import 'package:fbs_app/ui/widgets/fbs_button.dart';
import 'package:fbs_app/ui/widgets/main_app_bg.dart';
import 'package:flutter/material.dart';
import 'package:fbs_app/ui/common/app_theme.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:intl/intl.dart';
import 'package:states_rebuilder/states_rebuilder.dart';

class SessionEditPage extends StatefulWidget {
  SessionEditPage(this.currentSession);

  final Session currentSession;

  _SessionEdit createState() => _SessionEdit(currentSession);
}

class _SessionEdit extends State<SessionEditPage> {
  TextEditingController descriptionController;

  final user = Injector.get<AuthenticationService>().user;
  final Session session;

  bool _validate =
      false; //check if all essential text fields have been filled out
  //to choose in calendar
  DateTime selectedDate;
  TimeOfDay selectedTime;
  DateTime selectedEndDate;
  TimeOfDay selectedEndTime;

  //to get duration
  DateTime start;
  DateTime end;
  Duration duration;

  _SessionEdit(this.session) {
    start = DateTime.fromMillisecondsSinceEpoch((session.start * 1000).round());
    end = DateTime.fromMillisecondsSinceEpoch((session.end * 1000).round());
    duration = end.difference(start);
    selectedDate = new DateTime(start.year, start.month, start.day);
    selectedEndDate = new DateTime(end.year, end.month, end.day);
    selectedTime = new TimeOfDay(hour: start.hour, minute: start.minute);
    selectedEndTime = new TimeOfDay(hour: end.hour, minute: end.minute);
    descriptionController =
    new TextEditingController(text: session.description);
  }

  String _showDuration(Duration duration) {
    String twoDigits(int n) => n.toString().padLeft(2, "0");
    String twoDigitHours = twoDigits(duration.inHours.remainder(24));
    String twoDigitMinutes = twoDigits(duration.inMinutes.remainder(60));
    return duration.inDays.toString() +
        " Tage, $twoDigitHours Stunden, $twoDigitMinutes Minuten";
  }

  //select start date
  Future<Null> _selectDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime(2020, 1),
      lastDate: DateTime(2040, 12),
      initialDate: (selectedDate == null) ? new DateTime.now() : selectedDate,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null)
      setState(() {
        selectedDate = picked;
        if (selectedDate != null && selectedTime != null) {
          start = new DateTime(selectedDate.year, selectedDate.month,
              selectedDate.day, selectedTime.hour, selectedTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  Future<Null> _selectEndDate(BuildContext context) async {
    final DateTime picked = await showDatePicker(
      context: context,
      firstDate: DateTime(2020, 1),
      lastDate: DateTime(2040, 12),
      initialDate:
      (selectedEndDate == null) ? new DateTime.now() : selectedEndDate,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null)
      setState(() {
        selectedEndDate = picked;
        if (selectedEndDate != null && selectedEndTime != null) {
          end = new DateTime(
              selectedEndDate.year,
              selectedEndDate.month,
              selectedEndDate.day,
              selectedEndTime.hour,
              selectedEndTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  Future<void> _selectTime(BuildContext context) async {
    //0: start; 1: end
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime: (selectedTime == null)
          ? TimeOfDay.fromDateTime(DateTime.now())
          : selectedTime,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null)
      setState(() {
        selectedTime = picked;
        if (selectedDate != null &&
            TimeOfDay.fromDateTime(DateTime.now()) != null) {
          start = new DateTime(selectedDate.year, selectedDate.month,
              selectedDate.day, selectedTime.hour, selectedTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  Future<void> _selectEndTime(BuildContext context) async {
    //0: start; 1: end
    final TimeOfDay picked = await showTimePicker(
      context: context,
      initialTime:
      (selectedEndTime == null) ? TimeOfDay.now() : selectedEndTime,
      builder: (BuildContext context, Widget child) {
        return Theme(
          data: ThemeData.light().copyWith(
            primaryColor: green,
            accentColor: green,
            colorScheme: ColorScheme.light(primary: green),
            buttonTheme: ButtonThemeData(textTheme: ButtonTextTheme.primary),
          ),
          child: child,
        );
      },
    );
    if (picked != null && picked != selectedEndTime)
      setState(() {
        selectedEndTime = picked;
        if (selectedEndDate != null && selectedEndTime != null) {
          end = new DateTime(
              selectedEndDate.year,
              selectedEndDate.month,
              selectedEndDate.day,
              selectedEndTime.hour,
              selectedEndTime.minute);
          if (end != null && start != null) {
            duration = end.difference(start);
          }
        }
      });
  }

  void validate() {
    setState(() {
      _validate = (selectedDate == null ||
          selectedTime == null ||
          selectedEndDate == null ||
          selectedEndTime == null ||
          duration.isNegative);
    });
  }

  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () => FocusScope.of(context).unfocus(),
      child: Scaffold(
          body: SingleChildScrollView(
              child: Stack(children: [
        Container(
          height: MediaQuery.of(context).size.height,
          width: MediaQuery.of(context).size.width,
          child: MainAppBackground(
              headerHeight: (MediaQuery.of(context).size.height) / 3),
        ),
        SafeArea(
            child: SingleChildScrollView(
                child: Center(
                    child: Container(
                        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
                padding: EdgeInsets.all(25),
                child: Container(
                    padding: EdgeInsets.all(25),
                    decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.circular(12),
                    ),
                    constraints: ((MediaQuery.of(context).size.width) > 768)
                        ? BoxConstraints(minWidth: 100, maxWidth: 500)
                        : null,
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: [
                            Container(
                              constraints: ((MediaQuery.of(context).size.width -
                                          140) <
                                      272)
                                  ? BoxConstraints(
                                      maxWidth:
                                          (MediaQuery.of(context).size.width) -
                                              100 -
                                              40) //100 for padding, 40 for icon
                                  : null,
                              child: Text("Session bearbeiten",
                                  textAlign: TextAlign.left, style: headline2),
                            ),
                            Container(
                              width: 40,
                              child: InkResponse(
                                onTap: () {
                                  Navigator.pop(context);
                                },
                                child: CircleAvatar(
                                  backgroundColor: theme.focusColor,
                                  radius: 20,
                                  child: Icon(
                                    Icons.clear,
                                    color: Colors.white,
                                    //size: 20,
                                  ),
                                ),
                              ),
                            ),
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: Text(
                            "Beschreibung",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 16,
                                color: font_dark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Container(
                            padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                            constraints:
                                BoxConstraints(minHeight: 50, maxHeight: 100),
                            child: TextField(
                              textInputAction: TextInputAction.done,
                              controller: descriptionController,
                              textAlignVertical: TextAlignVertical.top,
                              maxLines: 5,
                              decoration: InputDecoration(
                                hintText: "Beschreibung eingeben (optional)",
                                focusedBorder: OutlineInputBorder(
                                    borderRadius: BorderRadius.circular(5.0),
                                    borderSide: BorderSide(
                                      color: green,
                                      style: BorderStyle.solid,
                                    )),
                                border: OutlineInputBorder(
                                  borderRadius: BorderRadius.circular(5.0),
                                  borderSide: BorderSide(
                                    color: green,
                                    style: BorderStyle.solid,
                                  ),
                                ),
                              ),
                            )),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 15),
                          child: Text(
                            "Freigabezeit und Dauer",
                            textAlign: TextAlign.start,
                            style: TextStyle(
                                fontSize: 16,
                                color: font_dark,
                                fontWeight: FontWeight.bold),
                          ),
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              flex: 4,
                              child: InkWell(
                                onTap: (() {
                                  FocusScope.of(context).unfocus();
                                  _selectDate(context);
                                }),
                                child: new InputDecorator(
                                  decoration: new InputDecoration(
                                    labelText: "Start-Datum",
                                  ),
                                  //   baseStyle: valueStyle,
                                  child: (selectedDate != null)
                                      ? Text(DateFormat('yyyy-MM-dd')
                                          .format(selectedDate))
                                      : Text(""),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: (MediaQuery.of(context).size.width) / 8,
                            ),
                            Expanded(
                              flex: 4,
                              child: InkWell(
                                onTap: (() {
                                  FocusScope.of(context).unfocus();
                                  _selectTime(context);
                                }),
                                child: new InputDecorator(
                                  decoration: new InputDecoration(
                                    labelText: "Start-Uhrzeit",
                                  ),
                                  //   baseStyle: valueStyle,
                                  child: new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      (selectedTime != null)
                                          ? new Text(
                                              selectedTime.format(context))
                                          : new Text(""),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          mainAxisSize: MainAxisSize.min,
                          children: [
                            Expanded(
                              flex: 4,
                              child: InkWell(
                                onTap: (() {
                                  FocusScope.of(context).unfocus();
                                  _selectEndDate(context);
                                }),
                                child: new InputDecorator(
                                  decoration: new InputDecoration(
                                    labelText: "End-Datum",
                                  ),
                                  //   baseStyle: valueStyle,
                                  child: (selectedEndDate != null)
                                      ? Text(DateFormat('yyyy-MM-dd')
                                          .format(selectedEndDate))
                                      : Text(""),
                                ),
                              ),
                            ),
                            SizedBox(
                              width: (MediaQuery.of(context).size.width) / 8,
                            ),
                            Expanded(
                              flex: 4,
                              child: InkWell(
                                onTap: (() {
                                  FocusScope.of(context).unfocus();
                                  _selectEndTime(context);
                                }),
                                child: new InputDecorator(
                                  decoration: new InputDecoration(
                                    labelText: "End-Uhrzeit",
                                  ),
                                  child: new Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    mainAxisSize: MainAxisSize.min,
                                    children: <Widget>[
                                      (selectedEndTime != null)
                                          ? new Text(
                                              selectedEndTime.format(context))
                                          : new Text(""),
                                    ],
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 15, 0, 0),
                          child: (duration != null)
                              ? Text("Dauer: " + _showDuration(duration))
                              : Text("Dauer: -"),
                        ),
                        _validate
                            ? Container(
                                padding: EdgeInsets.fromLTRB(0, 10, 0, 0),
                                child: Text(
                                  "Alle Felder sind auszufüllen. Endzeit muss später als Anfangszeit sein.",
                                  style: TextStyle(color: red, fontSize: 16),
                                ),
                              )
                            : Container(),
                        Container(
                          padding: EdgeInsets.fromLTRB(0, 25, 0, 0),
                          child: WhenRebuilderOr<SessionService>(
                            observe: () => RM.get<SessionService>(),
                            onWaiting: () => Center(
                              child: CircularProgressIndicator(
                                valueColor:
                                    new AlwaysStoppedAnimation<Color>(green),
                              ),
                            ),
                            dispose: (_, __) {},
                            builder: (_, sessionServiceRM) {
                              return Center(
                                child: FbsButton(
                                  text: "Bestätigen",
                                  type: "primary",
                                  size: "big",
                                  onPressed: () {
                                    validate();
                                    if (!_validate) {
                                      if (selectedDate != null &&
                                          selectedEndTime != null) {
                                        DateTime start = new DateTime(
                                            selectedDate.year,
                                            selectedDate.month,
                                            selectedDate.day,
                                            selectedTime.hour,
                                            selectedTime.minute);
                                        DateTime end = new DateTime(
                                            selectedEndDate.year,
                                            selectedEndDate.month,
                                            selectedEndDate.day,
                                            selectedEndTime.hour,
                                            selectedEndTime.minute);
                                        session.start =
                                            (start.millisecondsSinceEpoch /
                                                    1000)
                                                .round();
                                        session.end =
                                            (end.millisecondsSinceEpoch / 1000)
                                                .round();
                                        session.description =
                                            descriptionController.text;
                                        sessionServiceRM.setState(
                                            //create session
                                            (state) => state.updateSession(
                                                this.session,
                                                user.authToken,
                                                context),
                                            onData:
                                                (context, sessionServiceRM) {
                                          Navigator.pushReplacementNamed(
                                              context, 'me/sessions/detail',
                                              arguments:
                                              sessionServiceRM.session);
                                        }, onError: (context, e) {
                                          ErrorHandler.showErrorDialog(
                                              context, e);
                                        });
                                      }
                                    }
                                  },
                                ),
                              );
                            },
                          ),
                        ),
                      ],
                    ))),
          ],
        ))))),
      ]))),
    );
  }
}
