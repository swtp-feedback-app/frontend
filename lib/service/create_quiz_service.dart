import 'interfaces/i_api.dart';
import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/service/interfaces/i_api.dart';

class CreateQuizService {
  CreateQuizService({IApi api}) : _api = api;
  IApi _api;
  Quiz _fetchedQuiz;
  Session _fetchedSession;
  bool comesFromHome = false;

  Quiz get quiz => _fetchedQuiz;

  Session get session => _fetchedSession;

  ///
  /// Submits a new quiz. If a session is given, the session gets submitted as well.
  Future<void> createQuiz(Quiz quiz, Token authToken,
      [Session s, Course course]) async {
    _fetchedQuiz = quiz;
    try {
      _fetchedQuiz = await _api.submitQuiz(quiz, authToken);
      if (s != null) {
        await submitSession(authToken, s, course != null ? course : null);
      }
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Submits a new or edited question.
  Future<void> submitQuestion(
      Quiz q, Question question, Token authToken, bool update) async {
    try {
      if (!update) {
        //add question
        await _api.submitQuestion(q, question, authToken);
      } else {
        await _api.editQuestion(q, question, authToken);
      }
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Submits a session to the created quiz. If a course is given, assigns the session to a course.
  Future<void> submitSession(Token authToken, Session s,
      [Course course]) async {
    try {
      _fetchedSession = await _api.startSession(_fetchedQuiz, s, authToken);
      if (course != null) {
        await submitSessionToCourse(course, _fetchedSession.id, authToken);
      }
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Deletes the given question from the created quiz.
  Future<void> deleteQuestion(Question q, Token authToken) async {
    try {
      _api.deleteQuestion(_fetchedQuiz, q, authToken);
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Assigns a session to a course.
  Future<void> submitSessionToCourse(
      Course course, int sessionId, Token authToken) async {
    try {
      await _api.submitCourseSession(course, sessionId, authToken);
    } catch (e) {
      throw e;
    }
  }
}
