import 'package:fbs_app/domain/entities/course.dart';
import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/domain/entities/session.dart';
import 'package:fbs_app/domain/entities/token.dart';
import 'package:flutter/material.dart';
import 'interfaces/i_api.dart';
import 'package:fbs_app/domain/entities/statistics.dart';

class UserSessionStatisticsService {
  UserSessionStatisticsService({IApi api}) : _api = api;
  IApi _api;
  Statistics _fetchedStats;

  Statistics get stats => _fetchedStats;

  Future<void> getStats(Session session, {Token authToken}) async {
    try {
      _fetchedStats = authToken == null
          ? await _api.getUserSessionStatistics(session, session.sessToken)
          : await _api.getUserSessionStatisticsAuth(session, authToken);
    } catch (e) {
      throw e;
    }
  }
}

class SessionStatisticsService {
  SessionStatisticsService({IApi api}) : _api = api;
  IApi _api;
  SessionStatisticsOverview _sessionStatsOverview;
  MemoryImage _courseImage;
  MemoryImage _sessionImage;
  List<QuestionStatistic> _initQuestionStats;
  List<QuestionStatistic> _questionStats;
  Session _wordcloudSession;
  MemoryImage _wordCloudImage;

  bool search;

  SessionStatisticsOverview get stats => _sessionStatsOverview;

  MemoryImage get courseImage => _courseImage;

  MemoryImage get sessionImage => _sessionImage;

  MemoryImage get wordCloudImage => _wordCloudImage;

  List<QuestionStatistic> get questionStats => _questionStats;

  Future<void> sessStats(Session session, Token authToken) async {
    _sessionImage = null;
    try {
      _sessionImage = await _api.getSessionStatisticsImage(session, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> courseStats(Course course, Token authToken) async {
    _courseImage = null;
    try {
      _courseImage = await _api.getCourseStatisticsImage(course, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> getSessionStatsOverview(Session session, Token authToken) async {
    try {
      _sessionStatsOverview =
          await _api.getSessionStatisticsOverview(session, authToken);
    } catch (e) {
      throw e;
    }
  }

  Future<void> getSessionQuestionStats(Session session, Token authToken) async {
    try {
      _wordcloudSession = session;
      _initQuestionStats = await _api.getQuestionStatistics(session, authToken);
      _questionStats = _initQuestionStats;
    } catch (e) {
      throw e;
    }
  }

  Future<void> getSessionQuestionsStatsFilter(String value) async {
    try {
      _questionStats = _initQuestionStats
          .where((item) => item.question.question
              .toLowerCase()
              .contains(value.toLowerCase()))
          .toList();
    } catch (e) {
      throw e;
    }
  }

  Future<void> getWordCloudImage(int questionNumber, Token authToken) async {
    try {
      if (_wordcloudSession != null) {
        _wordCloudImage = await _api.getWordcloudImage(
            _wordcloudSession, questionNumber, authToken);
      }
    } catch (e) {
      throw e;
    }
  }
}

class UserTotalStatisticsService {
  UserTotalStatisticsService({IApi api}) : _api = api;
  IApi _api;
  UserStatistics _userStats;

  UserStatistics get stats => _userStats;

  Future<void> getUserStats(Token authToken) async {
    _userStats = await _api.getUserStats(authToken);
  }
}

class UserCourseStatisticsService {
  UserCourseStatisticsService({IApi api}) : _api = api;
  IApi _api;
  UserStatistics _userCourseStats;

  UserStatistics get stats => _userCourseStats;

  Future<void> getUserCourseStats(Token authToken, int courseId) async {
    _userCourseStats = await _api.getUserCourseStats(authToken, courseId);
  }
}
