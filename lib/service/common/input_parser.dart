import 'package:fbs_app/service/exceptions/input_exception.dart';

class InputParser {
  static int coursePassword(String passwordText) {
    int parsedPassword = int.tryParse(passwordText);
    if (parsedPassword == null) throw InvalidCredentialsInputException();
    return parsedPassword;
  }

  static int sessionId(String sessionIdText) {
    int parsedSessionId = int.tryParse(sessionIdText);
    if (parsedSessionId == null || sessionIdText.length != 8)
      throw InvalidSessionIdInputException();
    return parsedSessionId;
  }
}
