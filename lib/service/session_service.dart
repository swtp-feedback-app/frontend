import 'package:fbs_app/service/common/input_parser.dart';
import 'package:flutter/cupertino.dart';
import 'interfaces/i_api.dart';
import 'package:fbs_app/domain/entities/index.dart';
import 'package:fbs_app/service/interfaces/i_api.dart';

class SessionService {
  SessionService({IApi api}) : _api = api;
  IApi _api;
  Session _fetchedSession;

  bool comesFromCourse = false;
  Course course;

  Session get session => _fetchedSession;


  ///
  /// Get a new session with a session id.
  ///
  Future<void> getSession(String sessionId) async {
    _fetchedSession = null; // reset previous session
    try {
      int parsedSessionId = InputParser.sessionId(sessionId);
      _fetchedSession = await _api.getQuizInfo(parsedSessionId);
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Get token for the given session and for a specific user. If received token is valid, try to get the questions for the session's quiz.
  ///
  Future<void> getSessionToken(Session s, Token authToken) async {
    try {
      _fetchedSession?.sessToken = null; // reset previous session
      _fetchedSession = await _api.getSessionToken(s, s.getMode(), authToken);
      await getSessionQuestions(_fetchedSession);
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Get questions for the given session's quiz.
  ///
  Future<void> getSessionQuestions(Session s) async {
    try {
      _fetchedSession = s;
      _fetchedSession.quiz.questions = await _api.getSessionQuestions(
          _fetchedSession, _fetchedSession.sessToken);
      _fetchedSession.quiz.questions
          .sort((a, b) => a.number.compareTo(b.number)); //sort by number!
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Submit an edited session.
  ///
  Future<void> updateSession(
      Session s, Token authToken, BuildContext context) async {
    _fetchedSession = null; // Important!
    try {
      _fetchedSession = await _api.editSession(s, authToken);
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Submit answer when participation in a session.
  ///
  Future<void> submitAnswer(Session s, int questionNumber, List<Answer> answers) async {
    try {
      await _api.submitAnswer(
          s, questionNumber, s.sessToken, answers); // Needs to be await!
    } catch (e) {
      throw e;
    }
  }

  ///
  /// Reset the session.
  void resetSession() {
    _fetchedSession = null;
  }

  ///
  /// Set the session.
  void setSession(Session session) {
    _fetchedSession = session;
  }

  ///
  /// Delete the given session, if proper authorization is there.
  void deleteSession(Session session, Token authToken) async {
    try {
      _api.deleteSession(session, authToken);
    } catch (e) {
      throw e;
    }
  }
}
